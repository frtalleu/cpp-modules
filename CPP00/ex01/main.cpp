/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/26 14:30:34 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/26 14:30:39 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "phonebook.hpp"
#include <iostream>
#include <string>
#include <cstdlib>

int add(phonebook book[8], int count){
    if (count == 8){
        std::cout << "No space left" << std::endl;
        return (count);
    }
    book[count].set_phonebook();
    return (count + 1);
}

void aff_param(std::string str){
    int i = str.length();
    int j = 0;
    if (i <= 10){
        while (j < 10 - i) {
            std::cout << " ";
            j++;
        }
        std::cout << str;
    }
    else {
        std::cout << str.substr(0, 9) << ".";
    }

}

void print_param(phonebook book){
    std::string first_name;
    std::string last_name;
    std::string login;
    first_name = book.get_phonebook(0);
    last_name = book.get_phonebook(1);
    login = book.get_phonebook(3);
    aff_param(first_name);
    std::cout << "|";
    aff_param(last_name);
    std::cout << "|";
    aff_param(login);
    std::cout << "|";
    std::cout << std::endl;
}

void aff_book(phonebook book[8], int count)
{
    int i = 0;
    std::cout << "---------------------------------------------" << std::endl;
    std::cout << "|   index  |first name| last name|   login  |" << std::endl;
    while (i < count){
        std::cout << "|         " << i << "|";
        print_param(book[i]);
        i++;
    }
    std::cout << "---------------------------------------------" << std::endl;
}

void aff_page(phonebook book){
    std::cout << "first name : " << book.get_phonebook(0) << std::endl;
    std::cout << "last name : " << book.get_phonebook(1) << std::endl;
    std::cout << "nickname : " << book.get_phonebook(2) << std::endl;
    std::cout << "login : " << book.get_phonebook(3) << std::endl;
    std::cout << "postal address : " << book.get_phonebook(4) << std::endl;
    std::cout << "email address : " << book.get_phonebook(5) << std::endl;
    std::cout << "phone number : " << book.get_phonebook(6) << std::endl;
    std::cout << "birthday date : " << book.get_phonebook(7) << std::endl;
    std::cout << "favorite meal : " << book.get_phonebook(8) << std::endl;
    std::cout << "underwear color : " << book.get_phonebook(9) << std::endl;
    std::cout << "darkest secret : " << book.get_phonebook(10) << std::endl;
}

bool is_digit(std::string str)
{
    for(unsigned int i = 0; i < str.length(); i++) {
        if (str[i] < '0' || str[i] > '9')
            return (false);
    }
    return (true);
}

void search(phonebook book[8], int count) {
    std::string str;
    int index;

    aff_book(book, count);
    if (count == 0)
        return;

    while (!std::cin.eof()) {
        std::cout << "Enter index of a page" << std::endl;
        getline(std::cin, str);
        if (str.length() == 0 || is_digit(str) == false)
            continue;
        index = atoi(str.c_str());
        if (index < count) {
            aff_page(book[index]);
            break;
        }
    }
}

int main(){
    phonebook book[8];
    int count = 0;
    std::string cmd;

    while (1)
    {
        if (std::cin.eof())
            break;
        std::cout << "Enter a command" << std::endl;
        std::cout << ">> : ";
        getline(std::cin, cmd);
        if (cmd.compare("SEARCH") == 0)
            search(book, count);
        else if (cmd.compare("ADD") == 0)
            count = add(book, count);
        else if (cmd.compare("EXIT") == 0)
            break;
    }
}
