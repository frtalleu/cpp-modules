/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phonebook.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/26 14:30:57 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/26 14:31:01 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CPP_PHONEBOOK_HPP
#define CPP_PHONEBOOK_HPP
#include <string>

class phonebook {
private:
    std::string first_name;
    std::string last_name;
    std::string nickname;
    std::string login;
    std::string postal_address;
    std::string address;
    std::string phone_number;
    std::string birthday_date;
    std::string favorite_meal;
    std::string underwear_color;
    std::string darkest_secret;
public :
    void set_phonebook();
    std::string  get_phonebook(int i) const;
};


#endif //CPP_PHONEBOOK_HPP
