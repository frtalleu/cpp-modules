/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phonebook.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/26 14:30:45 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/26 14:30:50 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "phonebook.hpp"
#include <iostream>

void phonebook::set_phonebook() {
            std::cout << "Enter first name :" << std::endl;
            getline(std::cin, this->first_name);
            std::cout << "Enter last name :" << std::endl;
            getline(std::cin, this->last_name);
            std::cout << "Enter nickname :" << std::endl;
            getline(std::cin, this->nickname);
            std::cout << "Enter login :" << std::endl;
            getline(std::cin, this->login);
            std::cout << "Enter postal address :" << std::endl;
            getline(std::cin, this->postal_address);
            std::cout << "Enter email address :" << std::endl;
            getline(std::cin, this->address);
            std::cout << "Enter phone number :" << std::endl;
            getline(std::cin, this->phone_number);
            std::cout << "Enter birthday date :" << std::endl;
            getline(std::cin, this->birthday_date);
            std::cout << "Enter favorite meal :" << std::endl;
            getline(std::cin, this->favorite_meal);
            std::cout << "Enter underwear color :" << std::endl;
            getline(std::cin, this->underwear_color);
            std::cout << "Enter darkest secret :" << std::endl;
            getline(std::cin, this->darkest_secret);
}

std::string phonebook::get_phonebook(int i) const {
    switch (i) {
        case 0:
            return(this->first_name);
        case 1:
            return(this->last_name);
        case 2:
            return(this->nickname);
        case 3:
            return(this->login);
        case 4:
            return(this->postal_address);
        case 5:
            return(this->address);
        case 6:
            return(this->phone_number);
        case 7:
            return(this->birthday_date);
        case 8:
            return(this->favorite_meal);
        case 9:
            return(this->underwear_color);
        case 10:
            return(this->darkest_secret);
    }
    return("error");
}
