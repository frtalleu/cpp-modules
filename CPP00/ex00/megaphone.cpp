/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   megaphone.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/24 14:07:01 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/24 14:07:03 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string>
#include <iostream>

int main(int ac, char **av)
{
   int j = 0;
   int i = 1;
   if (ac == 1){
       std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *" << std::endl;
        return(0) ;
   }
   while (av[i])
   {
       while (av[i][j])
       {
           std::cout << (char) std::toupper(av[i][j]);
           j++;
       }
       if (av[i + 1])
           std::cout << " ";
       j = 0;
       i++;
   }
   std::cout << std::endl;
}
