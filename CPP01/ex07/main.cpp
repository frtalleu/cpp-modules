/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/29 11:19:17 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/29 11:19:22 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string>
#include <iostream>
#include <fstream>

int main(int ac, char **av)
{
    if (ac != 4) {
        std::cout << "Wrong number of args" << std::endl;
        return (0);
    }
    std::string to_remove(av[2]);
    std::string to_add(av[3]);
    std::string file_name(av[1]);
    if (to_remove.length() == 0 || to_add.length() == 0 || file_name.length() == 0)
    {
        std::cout << "Don't give empty string please" << std::endl;
        return (0);
    }
    std::ifstream ifs;
    ifs.open(file_name.c_str());
    file_name += ".replace";
    std::ofstream ofs;
    ofs.open(file_name.c_str());
    std::string read;
    std::string write;
    while(getline(ifs, read)){
        write.append(read);
        write.append("\n");
    }
    size_t pos = 0;
    ifs.close();
    if (to_remove.compare(to_add) != 0) {
        while ((pos = write.find(to_remove, pos)) != std::string::npos) {
            write.replace(write.find(to_remove, pos), to_remove.length(), to_add);
            pos += to_add.length();
        }
    }
    ofs << write;
    ofs.close();
}
