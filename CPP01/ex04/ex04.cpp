/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex04.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/28 13:27:39 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/28 13:27:42 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string>
#include <iostream>

int main()
{
    std::string str("HI THIS IS BRAIN");
    std::string* strPtr = &str;
    std::string& strRef = str;
    std::cout << "Print the string with the pointer" << std::endl;
    std::cout << *strPtr << std::endl;
    std::cout << "Print the string with the reference" << std::endl;
    std::cout << strRef << std::endl;
}
