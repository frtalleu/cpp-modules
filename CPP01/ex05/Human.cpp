/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/28 14:27:33 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/28 14:27:37 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Human.hpp"
#include "Brain.hpp"
#include <string>
#include <iostream>

const Brain& Human::getBrain(void) const {
    return (this->brain);
}
std::string Human::identify(void) const{
    return (this->brain.identify());
}
