/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/28 14:27:15 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/28 14:27:19 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Brain.hpp"
#include <sstream>
#include <string>
#include <iostream>

std::string Brain::identify() const {
    std::ostringstream sstr;
    sstr << (const void *)this;
    return (sstr.str());
}
