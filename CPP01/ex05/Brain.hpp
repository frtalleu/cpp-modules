/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/28 14:27:23 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/28 14:27:27 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CPP_BRAIN_HPP
#define CPP_BRAIN_HPP

#include <string>
#include <iostream>

class Brain {
    public:
        std::string identify(void) const;
};


#endif //CPP_BRAIN_HPP
