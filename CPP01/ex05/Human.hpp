/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/28 14:27:48 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/28 14:27:52 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CPP_HUMAN_HPP
#define CPP_HUMAN_HPP

#include <string>
#include <iostream>
#include "Brain.hpp"

class Human {
    private:
        Brain  brain;
    public:
        std::string identify(void) const;
        const Brain& getBrain(void) const;
};


#endif //CPP_HUMAN_HPP
