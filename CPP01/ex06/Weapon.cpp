/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/28 15:14:34 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/28 15:14:39 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Weapon.hpp"
#include <string>

Weapon::Weapon(std::string t) : type(t){}

const std::string& Weapon::getType() const {
    return (this->type);
}

void Weapon::setType(std::string t) {
    this->type = t;
}
