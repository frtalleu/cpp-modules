/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/28 15:14:44 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/28 15:14:48 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CPP_WEAPON_HPP
#define CPP_WEAPON_HPP

#include <string>
#include <iostream>

class Weapon {
    private:
        std::string type;
    public:
        Weapon(std::string t);
        const std::string& getType(void) const;
        void setType(std::string t);
};


#endif //CPP_WEAPON_HPP
