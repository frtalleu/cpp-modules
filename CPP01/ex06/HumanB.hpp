/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/28 15:14:20 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/28 15:14:25 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CPP_HUMANB_HPP
#define CPP_HUMANB_HPP

#include "Weapon.hpp"
#include <string>
#include <iostream>

class HumanB {
    private:
        Weapon *weapon;
        std::string name;
    public:
        HumanB(std::string n);
        void setWeapon(Weapon &w);
        void attack(void) const;
};


#endif //CPP_HUMANB_HPP
