/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/28 15:13:55 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/28 15:14:00 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CPP_HUMANA_HPP
#define CPP_HUMANA_HPP

#include <string>
#include "Weapon.hpp"
#include <iostream>

class HumanA {
    private:
        std::string name;
        Weapon& weapon;
    public:
        HumanA(std::string n, Weapon& w);
        void attack(void) const;
};


#endif //CPP_HUMANA_HPP
