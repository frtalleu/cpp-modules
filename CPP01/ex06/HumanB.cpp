/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/28 15:14:03 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/28 15:14:09 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanB.hpp"
#include <string>
#include "Weapon.hpp"

HumanB::HumanB(std::string n) : name(n){}

void HumanB::setWeapon(Weapon &w) {
    this->weapon = &w;
}

void HumanB::attack(void) const {
    std::cout << this->name << " attacks with his " << this->weapon->getType() << std::endl;
}
