/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/28 15:13:42 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/28 15:13:48 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanA.hpp"
#include <iostream>
#include <string>
#include "Weapon.hpp"

HumanA::HumanA(std::string n, Weapon &w): name(n), weapon(w) {}

void HumanA::attack() const {
    std::cout << this->name << " attacks with his " << this->weapon.getType() << std::endl;
}
