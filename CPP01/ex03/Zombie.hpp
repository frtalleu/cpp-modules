/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 16:37:42 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/27 16:37:46 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CPP_ZOMBIE_HPP
#define CPP_ZOMBIE_HPP

#include <string>
#include <iostream>

class Zombie {
    private:
        std::string name;
        std::string type;
    public :
        void announce(void) const;
        void set_names(std::string const names);
        void set_types(std::string const types);

};
#endif //CPP_ZOMBIE_H
