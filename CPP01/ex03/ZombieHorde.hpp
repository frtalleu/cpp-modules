/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/28 13:26:17 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/28 13:26:23 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CPP_ZOMBIEHORDE_HPP
#define CPP_ZOMBIEHORDE_HPP

#include "Zombie.hpp"
#include <iostream>
#include <string>

class ZombieHorde {
    private:
        Zombie *Zombie_gang;
        int number_zombie;
    public:
        ZombieHorde(int n);
        ~ZombieHorde(void);
        void announce(void) const;
};


#endif //CPP_ZOMBIEHORDE_HPP
