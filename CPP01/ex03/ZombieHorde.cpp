/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/28 13:25:49 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/28 13:25:54 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieHorde.hpp"
#include <cstdlib>

ZombieHorde::ZombieHorde(int n) : number_zombie(n){
    const char *names[10] = {"Leo", "Pas Leo", "FL", "Sans nom", "Vraiment pas Leo", "ZOMBIE GOD", "NONO", "Noelle", "Pas tres Nono", "BENNY"};
    this->Zombie_gang = new Zombie[n];
    for(int i = 0; i < n; i++)
    {
        Zombie_gang[i].set_names(names[std::rand() % 10]);
        Zombie_gang[i].set_types("Horde");
    }
}

 ZombieHorde::~ZombieHorde(void){
    delete [] this->Zombie_gang;
}

void ZombieHorde::announce(void) const
{
    for(int i = 0; i < this->number_zombie; i++){
        this->Zombie_gang[i].announce();
    }
}
