/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 17:11:39 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/27 17:11:43 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include "ZombieEvent.hpp"
#include <cstdlib>

void randomChump(){
    const char *names[10] = {"Leo", "Pas Leo", "FL", "Sans nom", "Vraiment pas Leo", "ZOMBIE GOD", "NONO", "Noelle", "Pas tres Nono", "BENNY"};
    Zombie zonzon(names[std::rand() % 10], "clone");
    zonzon.announce();
}

int main(){
    ZombieEvent event;
    Zombie *tmp;
    event.setZombieType("VERY VERY BIIIIIIG");
    for(int i = 0; i < 5; i++){
        randomChump();
    }
    for(int i = 0; i < 10; i++){
        tmp = event.newZombie("Carlos");
        tmp->announce();
        delete tmp;
        tmp = event.newZombie("Francky");
        tmp->announce();
        delete tmp;
    }
}