/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 16:56:24 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/27 16:56:30 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieEvent.hpp"

void ZombieEvent::setZombieType(std::string type_add) {
    this->type = type_add;
}

Zombie *ZombieEvent::newZombie(std::string name) const{
    Zombie *zombie = new Zombie(name, this->type);
    return (zombie);
}