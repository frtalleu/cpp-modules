/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 14:44:20 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/27 14:44:21 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"
#include <iostream>
#include <string>

void ponyOnTheStack(void){
    Pony stack = Pony("Stack", 80, 190, "blue");
    stack.make_it_run();
    stack.make_it_sleep();
    stack.make_it_eat();
}

void ponyOnTheHeap(void){
    Pony *heap = new Pony("Heap", 100, 200, "green");
    heap->make_it_run();
    heap->make_it_sleep();
    heap->make_it_eat();
    delete heap;
}

int main(){
    ponyOnTheStack();
    ponyOnTheHeap();
}
