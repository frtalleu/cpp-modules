/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pony.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 14:35:38 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/27 14:35:45 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"
#include <iostream>
#include <string>

Pony::Pony(std::string n, int w, int h, std::string c) : name(n), weight(w), height(h), color(c) {
    std::cout << "Pony call " << this->name << " is born! :D" << std::endl;
    std::cout << "Height : " << this->height << std::endl;
    std::cout << "Weight : " << this->weight << std::endl;
    std::cout << "Color : " << this->color << std::endl;
}

Pony::~Pony (void){
    std::cout << "Pony call " << this->name << " is dead ! :'(" << std::endl;
}

void Pony::make_it_run (void){
    std::cout << "Pony call " << this->name << " is running !" << std::endl;
}

void Pony::make_it_eat (void){
    std::cout << "Pony call " << this->name << " is eating !" << std::endl;
}

void Pony::make_it_sleep (void){
    std::cout << "Pony call " << this->name << " is sleeping !" << std::endl;
}