/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pony.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 14:35:23 by frtalleu          #+#    #+#             */
/*   Updated: 2021/04/27 14:35:28 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CPP_PONY_H
#define CPP_PONY_H

#include <iostream>
#include <string>

class Pony {
    private:
        std::string name;
        int weight;
        int height;
        std::string color;
    public:
       Pony(std::string n, int w, int h, std::string c);
       ~Pony(void);
       void make_it_run(void);
       void make_it_eat(void);
       void make_it_sleep(void);
};


#endif //CPP_PONY_H
