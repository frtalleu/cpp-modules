//
// Created by Francois-louis TALLEUX on 5/28/21.
//
#include "Sorcerer.hpp"
#include "Victim.hpp"
#include "Peon.hpp"
#include "Minion.hpp"
#include "string"
#include "ostream"

int main()
{
    Sorcerer robert("Robert", "the Magnificent");
    Sorcerer robert_bis(robert);
    Victim jim("Jimmy");
    Victim jim_bis(jim);
    Victim vitime_default;
    Peon joe("Joe");
    Peon joe_bis(joe);
    Peon peon_default;
    Minion jul("Jul");
    Minion jul_bis(jul);
    Minion minion_default;
    std::cout << robert << jim << joe << jul;
    robert.polymorph(jul);
    robert.polymorph(jim);
    robert.polymorph(joe);
    return 0;
}