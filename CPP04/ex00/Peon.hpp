//
// Created by Francois-louis TALLEUX on 5/24/21.
//

#ifndef CPP_MODULES_PEON_HPP
#define CPP_MODULES_PEON_HPP
#include "Victim.hpp"
#include "ostream"
#include "string"

class Peon : public Victim{
    public:
        Peon(void);
        Peon(const Peon &src);
        Peon(std::string names);
        virtual ~Peon();
        Peon &operator=(const Peon &src);
        void getPolymorphed(void) const;
};

std::ostream &operator<<(std::ostream &os, const Peon &rhs);

#endif //CPP_MODULES_PEON_HPP