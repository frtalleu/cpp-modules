//
// Created by Francois-louis TALLEUX on 5/24/21.
//

#include "Peon.hpp"
#include "Victim.hpp"
#include "string"
#include "ostream"

Peon::Peon(void) : Victim(){
   std::cout << "Zog zog." << std::endl;
}


Peon::Peon(const Peon &src) : Victim(src){
    std::cout << "Zog zog." << std::endl;
}

Peon::Peon(std::string names) : Victim(names){
   std::cout << "Zog zog." << std::endl;
}

Peon::~Peon() {
    std::cout << "Bleuark..." << std::endl;
}

void Peon::getPolymorphed() const{
    std::cout << this->get_name() << " has been turned into a pink pony!" << std::endl;
}

Peon &Peon::operator=(const Peon &src) {
    this->set_name(src.get_name());
    return (*this);
}

std::ostream &operator<<(std::ostream &os, const Peon &rhs) {
    os << "I'm " << rhs.get_name() << " and I like otters!" << std::endl;
    return os;
}