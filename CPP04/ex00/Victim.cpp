//
// Created by Francois-louis TALLEUX on 5/24/21.
//

#include "Victim.hpp"
#include <string>
#include <iostream>

Victim::Victim(void) {
    this->name = "Default";
    std::cout << "Some random victim called " << this->name << " just appeared!" << std::endl;
}

Victim::Victim(const Victim &src) {
    this->name = src.get_name();
    std::cout << "Some random victim called " << this->name << " just appeared!" << std::endl;
}

Victim::Victim(std::string names) {
    this->name = names;
    std::cout << "Some random victim called " << this->name << " just appeared!" << std::endl;
}

Victim::~Victim() {
    std::cout << "Victim " << this->name << " just died for no apparent reason!" << std::endl;
}

std::string Victim::get_name() const{
    return (this->name);
}

void Victim::set_name(std::string names) {
    this->name = names;
}

void Victim::getPolymorphed() const{
    std::cout << this->name << " has been turned into a cute little sheep!" << std::endl;
}

Victim &Victim::operator=(const Victim &src) {
    this->name = src.get_name();
    return (*this);
}

std::ostream &operator<<(std::ostream &os, const Victim &rhs) {
    os << "I'm " << rhs.get_name() << " and I like otters!" << std::endl;
    return os;
}