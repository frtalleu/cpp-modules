//
// Created by Francois-louis TALLEUX on 5/24/21.
//

#include "Sorcerer.hpp"
#include "string"
#include "iostream"
#include "Victim.hpp"

Sorcerer::Sorcerer(void) {
    this->name = "Default";
    this->title = "the default";
    std::cout << this->name << ", " << this->title << ", is born!" << std::endl;
}

Sorcerer::Sorcerer(const Sorcerer &src) {
    this->name = src.get_name();
    this->title = src.get_title();
    std::cout << this->name << ", " << this->title << ", is born!" << std::endl;
}

Sorcerer::Sorcerer(std::string names, std::string titles){
    this->name = names;
    this->title = titles;
    std::cout << this->name << ", " << this->title << ", is born!" << std::endl;
}

Sorcerer::~Sorcerer() {
    std::cout << this->name << ", " << this->title << ", is dead. Consequences will never be the same!" << std::endl;
}

std::string Sorcerer::get_name() const{
    std::string tmp = this->name;
    return tmp;
}

std::string Sorcerer::get_title() const{
    std::string tmp = this->title;
    return tmp;
}

void Sorcerer::polymorph(const Victim &target) {
    target.getPolymorphed();
}

std::ostream &operator<<(std::ostream &os, const Sorcerer &rhs)
{
    os << "I am " << rhs.get_name() << ", " << rhs.get_title() << ", and I like ponies!" << std::endl;
    return os;
}

Sorcerer &Sorcerer::operator=(const Sorcerer &src) {
    this->name = src.get_name();
    this->title = src.get_title();
    return (*this);
}