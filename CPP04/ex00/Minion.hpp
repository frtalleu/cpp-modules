//
// Created by Francois-louis TALLEUX on 5/24/21.
//

#ifndef CPP_MODULES_MINION_HPP
#define CPP_MODULES_MINION_HPP
#include "Victim.hpp"
#include "ostream"
#include "string"

class Minion : public Victim{
    public:
        Minion(void);
        Minion(const Minion &src);
        Minion(std::string names);
        virtual ~Minion();
        Minion &operator=(const Minion &src);
        void getPolymorphed(void) const;
};

std::ostream &operator<<(std::ostream &os, const Minion &rhs);

#endif //CPP_MODULES_PEON_HPP