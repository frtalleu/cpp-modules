//
// Created by Francois-louis TALLEUX on 5/24/21.
//

#include "Minion.hpp"
#include "Victim.hpp"
#include "string"
#include "ostream"

Minion::Minion(void) : Victim(){
   std::cout << "Zeg zeg." << std::endl;
}


Minion::Minion(const Minion &src) : Victim(src){
    std::cout << "Zeg zeg." << std::endl;
}

Minion::Minion(std::string names) : Victim(names){
   std::cout << "Zeg zeg." << std::endl;
}

Minion::~Minion() {
    std::cout << "Burk..." << std::endl;
}

void Minion::getPolymorphed() const{
    std::cout << this->get_name() << " has been turned into a cute rabbit!" << std::endl;
}

Minion &Minion::operator=(const Minion &src) {
    this->set_name(src.get_name());
    return (*this);
}

std::ostream &operator<<(std::ostream &os, const Minion &rhs) {
    os << "I'm " << rhs.get_name() << " and I like nothing!" << std::endl;
    return os;
}