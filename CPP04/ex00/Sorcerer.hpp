//
// Created by Francois-louis TALLEUX on 5/24/21.
//

#ifndef CPP_MODULES_SORCERER_HPP
#define CPP_MODULES_SORCERER_HPP
#include "string"
#include "iostream"
#include "Victim.hpp"

class Sorcerer {
    public:
        Sorcerer(const Sorcerer &src);
        Sorcerer(std::string names, std::string titles);
        ~Sorcerer();
        Sorcerer &operator=(const Sorcerer &src);
        std::string get_name() const;
        std::string get_title() const;
        void polymorph(Victim const &target);
    private:
        Sorcerer(void);
        std::string name;
        std::string title;
};

std::ostream &operator<<(std::ostream &os, const Sorcerer &rhs);

#endif //CPP_MODULES_SORCERER_HPP