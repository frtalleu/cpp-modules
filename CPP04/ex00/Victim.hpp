//
// Created by Francois-louis TALLEUX on 5/24/21.
//

#ifndef CPP_MODULES_VICTIM_HPP
#define CPP_MODULES_VICTIM_HPP

#include "string"
#include "iostream"

class Victim {
    public:
        Victim(void);
        Victim(const Victim &);
        Victim(std::string names);
        virtual ~Victim();
        Victim &operator=(const Victim &);
        std::string get_name() const;
        void set_name(std::string names);
        virtual void getPolymorphed(void) const;
    private:
        std::string name;
};

std::ostream &operator<<(std::ostream &os, const Victim &rhs);

#endif //CPP_MODULES_VICTIM_HPP