//
// Created by Francois-louis TALLEUX on 6/8/21.
//

#include "Bazooka.hpp"

Bazooka::Bazooka(void) {
    this->name = "Bazooka";
    this->AP = 20;
    this->damage = 50;
    this->attack_sound = "* BOOM *";
}

Bazooka::Bazooka(const Bazooka &src) {
    this->name = src.getName();
    this->AP = src.getAPCost();
    this->damage = src.getDamage();
    this->attack_sound = src.getAttack();
}

void Bazooka::attack() const {
    std::cout << this->getAttack() << std::endl;
}

Bazooka::~Bazooka() {}
