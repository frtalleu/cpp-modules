//
// Created by Francois-louis TALLEUX on 6/4/21.
//

#include "AWeapon.hpp"

AWeapon::AWeapon(void) {
    this->name = "Default";
    this->damage = 0;
    this->AP = 0;
    this->attack_sound = "";
}

AWeapon::AWeapon(const std::string &names, int apcost, int damage) {
    this->name = names;
    this->AP = apcost;
    this->damage = damage;
    this->attack_sound = "";
}

AWeapon::AWeapon(const AWeapon &src) {
    this->AP = src.getAPCost();
    this->damage = src.getDamage();
    this->name = src.getName();
    this->attack_sound = src.getAttack();
}

AWeapon::~AWeapon() {}

int AWeapon::getAPCost() const {
    int res = this->AP;
    return (res);
}

int AWeapon::getDamage() const {
    int res = this->damage;
    return (res);
}

std::string AWeapon::getAttack() const {
    std::string res = this->attack_sound;
    return (res);
}


std::string AWeapon::getName() const {
    std::string res = this->name;
    return (res);
}

AWeapon &AWeapon::operator=(const AWeapon &src) {
    this->name = src.getName();
    this->attack_sound = src.getAttack();
    this->damage = src.getDamage();
    this->AP = src.getAPCost();
    return (*this);
}