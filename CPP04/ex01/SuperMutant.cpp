//
// Created by Francois-louis TALLEUX on 6/11/21.
//

#include "SuperMutant.hpp"

SuperMutant::SuperMutant(void) : Enemy(170, "Super Mutant"){
    std::cout << "Gaaah. Me want smash heads!" << std::endl;
}

SuperMutant::SuperMutant(const SuperMutant &src) {
    this->HP = src.getHP();
    this->type = src.getType();
}

SuperMutant::~SuperMutant() {
    std::cout << "Aaargh..." << std::endl;
}

void SuperMutant::takeDamage(int amount) {
    amount -= 3;
    Enemy::takeDamage(amount);
    if (this->getHP() == 0)
        std::cout << "Aaargh..." << std::endl;
}