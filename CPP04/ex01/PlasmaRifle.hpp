//
// Created by Francois-louis TALLEUX on 6/8/21.
//

#ifndef CPP_MODULES_PLASMARIFLE_HPP
#define CPP_MODULES_PLASMARIFLE_HPP
#include "string"
#include "iostream"
#include "AWeapon.hpp"

class PlasmaRifle : public AWeapon {
public:
    PlasmaRifle(void);
    PlasmaRifle(const PlasmaRifle &src);
    virtual ~PlasmaRifle();
    using AWeapon::operator=;
    void attack() const;
};


#endif //CPP_MODULES_PLASMARIFLE_HPP