//
// Created by Francois-louis TALLEUX on 6/12/21.
//

#include "Character.hpp"

Character::Character(void) {
    this->name = "Default";
    this->weapon = NULL;
    this->AP = 40;
}

Character::Character(const std::string &names) {
    this->name = names;
    this->AP = 40;
    this->weapon = NULL;
}

Character::Character(const Character &src) {
    this->name = src.get_name();
    this->AP = src.get_AP();
    this->weapon = NULL;
}

Character::~Character() {}

void Character::equip(AWeapon *weapon1) {
    this->weapon = weapon1;
}

void Character::attack(Enemy *enemy) {
    if (this->weapon != NULL && this->AP - this->weapon->getAPCost() > 0) {
        std::cout << this->name << " attacks " << enemy->getType() << " with a " << this->weapon->getName() << std::endl;
        this->weapon->attack();
        enemy->takeDamage(this->weapon->getDamage());
        this->AP -= this->weapon->getAPCost();
    }
    else
        std::cout << this->name << " attacks " << enemy->getType() << " with nothing" << std::endl;

}

void Character::recoverAP() {
    if (this->AP + 10 >= 40)
        this->AP = 40;
    else
        this->AP += 10;
}

Character &Character::operator=(const Character &src) {
    this->name = src.get_name();
    this->AP = src.get_AP();
    return (*this);
}

AWeapon *Character::get_weapon() const{
    return (this->weapon);
}

std::string Character::get_name() const {
    return (this->name);
}

int Character::get_AP() const {
    return (this->AP);
}

std::ostream &operator<<(std::ostream &os, const Character &rhs)
{
    if (rhs.get_weapon() != NULL)
        os << rhs.get_name() << " has " << rhs.get_AP() << " AP and wields a " << rhs.get_weapon()->getName() << std::endl;
    else
        os << rhs.get_name() << " has " << rhs.get_AP() << " AP and is unarmed" << std::endl;
    return os;
}