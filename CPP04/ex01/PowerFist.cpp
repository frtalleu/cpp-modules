//
// Created by Francois-louis TALLEUX on 6/8/21.
//

#include "PowerFist.hpp"

PowerFist::PowerFist(void) {
    this->name = "Power Fist";
    this->AP = 8;
    this->damage = 50;
    this->attack_sound = "* pschhh... SBAM! *";
}

PowerFist::PowerFist(const PowerFist &src) {
    this->name = src.getName();
    this->AP = src.getAPCost();
    this->damage = src.getDamage();
    this->attack_sound = src.getAttack();
}

void PowerFist::attack() const {
    std::cout << this->getAttack() << std::endl;
}

PowerFist::~PowerFist() {}
