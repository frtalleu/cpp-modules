//
// Created by Francois-louis TALLEUX on 6/8/21.
//

#ifndef CPP_MODULES_BAZOOKA_HPP
#define CPP_MODULES_BAZOOKA_HPP
#include "string"
#include "iostream"
#include "AWeapon.hpp"

class Bazooka : public AWeapon {
public:
    Bazooka(void);
    Bazooka(const Bazooka &src);
    virtual ~Bazooka();
    using AWeapon::operator=;
    void attack() const;
};


#endif //CPP_MODULES_PLASMARIFLE_HPP