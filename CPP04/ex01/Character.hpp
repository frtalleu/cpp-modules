//
// Created by Francois-louis TALLEUX on 6/12/21.
//

#ifndef CPP_MODULES_CHARACTER_HPP
#define CPP_MODULES_CHARACTER_HPP
#include "AWeapon.hpp"
#include "Enemy.hpp"

class Character {
private:
    std::string name;
    int AP;
    AWeapon *weapon;
public:
    Character(std::string const &names);
    Character(void);
    Character(const Character &src);
    ~Character();
    Character &operator=(const Character &src);
    void recoverAP();
    void equip(AWeapon *weapon1);
    void attack(Enemy *enemy);
    std::string get_name(void) const;
    int get_AP(void) const;
    AWeapon *get_weapon(void) const;
};

std::ostream &operator<<(std::ostream &os, const Character &rhs);

#endif //CPP_MODULES_CHARACTER_HPP