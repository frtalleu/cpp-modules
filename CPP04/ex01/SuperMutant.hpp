//
// Created by Francois-louis TALLEUX on 6/11/21.
//

#ifndef CPP_MODULES_SUPERMUTANT_HPP
#define CPP_MODULES_SUPERMUTANT_HPP
#include "Enemy.hpp"
#include "string"
#include "iostream"

class SuperMutant : public Enemy{
public:
    SuperMutant(void);
    SuperMutant(const SuperMutant &src);
    virtual ~SuperMutant();
    void takeDamage(int amount);
    using Enemy::operator=;
};


#endif //CPP_MODULES_SUPERMUTANT_HPP