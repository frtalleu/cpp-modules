//
// Created by Francois-louis TALLEUX on 6/8/21.
//

#include "Alakablam.hpp"

Alakablam::Alakablam(void) {
    this->name = "Alakablam";
    this->AP = 1;
    this->damage = 5;
    this->attack_sound = "* ALAKABLAM *";
}

Alakablam::Alakablam(const Alakablam &src) {
    this->name = src.getName();
    this->AP = src.getAPCost();
    this->damage = src.getDamage();
    this->attack_sound = src.getAttack();
}

void Alakablam::attack() const {
    std::cout << this->getAttack() << std::endl;
}

Alakablam::~Alakablam() {}
