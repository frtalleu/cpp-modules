//
// Created by Francois-louis TALLEUX on 6/8/21.
//

#include "PlasmaRifle.hpp"

PlasmaRifle::PlasmaRifle(void) {
    this->name = "Plasma Rifle";
    this->AP = 5;
    this->damage = 21;
    this->attack_sound = "* piouuu piouuu piouuu *";
}

PlasmaRifle::PlasmaRifle(const PlasmaRifle &src) {
    this->name = src.getName();
    this->AP = src.getAPCost();
    this->damage = src.getDamage();
    this->attack_sound = src.getAttack();
}

void PlasmaRifle::attack() const {
    std::cout << this->getAttack() << std::endl;
}

PlasmaRifle::~PlasmaRifle() {}
