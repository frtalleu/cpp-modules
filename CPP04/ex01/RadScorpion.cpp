//
// Created by Francois-louis TALLEUX on 6/12/21.
//

#include "RadScorpion.hpp"

RadScorpion::RadScorpion(void) : Enemy(80, "RadScorpion"){
    std::cout << "* click click click *" << std::endl;
}

RadScorpion::RadScorpion(const RadScorpion &src) {
    this->HP = src.getHP();
    this->type = src.getType();
    std::cout << "* click click click *" << std::endl;

}

void RadScorpion::takeDamage(int amount) {
    Enemy::takeDamage(amount);
    if (this->getHP() == 0)
        std::cout << "* SPROTCH *" << std::endl;
}

RadScorpion::~RadScorpion() {
    std::cout << "* SPROTCH *" << std::endl;
}
