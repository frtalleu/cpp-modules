//
// Created by Francois-louis TALLEUX on 6/4/21.
//

#ifndef CPP_MODULES_AWEAPON_HPP
#define CPP_MODULES_AWEAPON_HPP
#include "string"
#include "iostream"

class AWeapon {
public:
    AWeapon(void);
    AWeapon(std::string const &names, int apcost, int damage);
    AWeapon(const AWeapon &src);
    virtual ~AWeapon();
    AWeapon &operator=(const AWeapon &src);
    int getAPCost() const;
    int getDamage() const;
    std::string getName() const;
    std::string getAttack() const;
    virtual void attack() const = 0;
protected:
    int AP;
    int damage;
    std::string name;
    std::string attack_sound;
};


#endif //CPP_MODULES_AWEAPON_HPP