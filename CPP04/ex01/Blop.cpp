//
// Created by Francois-louis TALLEUX on 6/12/21.
//

#include "Blop.hpp"

Blop::Blop(void) : Enemy(20, "Blop"){
    std::cout << "* BLOP *" << std::endl;
}

Blop::Blop(const Blop &src) {
    this->HP = src.getHP();
    this->type = src.getType();
    std::cout << "* BLOP *" << std::endl;

}

void Blop::takeDamage(int amount) {
    Enemy::takeDamage(amount);
    if (this->getHP() == 0)
        std::cout << "* SPLOOTCH *" << std::endl;
}

Blop::~Blop() {
    std::cout << "* SPLOOTCH *" << std::endl;
}
