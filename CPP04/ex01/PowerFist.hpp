//
// Created by Francois-louis TALLEUX on 6/8/21.
//

#ifndef CPP_MODULES_POWERFIST_HPP
#define CPP_MODULES_POWERFIST_HPP
#include "string"
#include "iostream"
#include "AWeapon.hpp"

class PowerFist : public AWeapon {
public:
    PowerFist(void);
    PowerFist(const PowerFist &src);
    virtual ~PowerFist();
    using AWeapon::operator=;
    void attack() const;
};


#endif //CPP_MODULES_POWERFIST_HPP