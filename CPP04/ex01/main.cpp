//
// Created by Francois-louis TALLEUX on 6/13/21.
//

#include "Enemy.hpp"
#include "AWeapon.hpp"
#include "Character.hpp"
#include "PlasmaRifle.hpp"
#include "PowerFist.hpp"
#include "RadScorpion.hpp"
#include "Blop.hpp"
#include "Alakablam.hpp"
#include "Bazooka.hpp"
#include "SuperMutant.hpp"
#include "string"
#include "iostream"

int main()
{
    Character* me = new Character("me");
    std::cout << *me;
    Enemy* b = new RadScorpion();
    Enemy* a = new SuperMutant;
    Enemy* c = new Blop();
    AWeapon* pr = new PlasmaRifle();
    AWeapon* pf = new PowerFist();
    AWeapon *blam = new Alakablam();
    AWeapon *bazooka = new Bazooka();
    me->equip(pr);
    std::cout << *me;
    me->equip(pf);
    me->attack(b);
    std::cout << *me;
    me->equip(pr);
    std::cout << *me;
    me->attack(b);
    std::cout << *me;
    me->attack(b);
    std::cout << *me;

    me->recoverAP();
    me->recoverAP();

    me->equip(pf);
    std::cout << *me;
    me->equip(pr);
    me->attack(a);
    std::cout << *me;
    me->equip(pf);
    std::cout << *me;
    me->attack(a);
    std::cout << *me;
    me->attack(a);
    std::cout << *me;
    me->attack(a);
    std::cout << *me;


    me->recoverAP();
    me->recoverAP();
    me->recoverAP();


    me->attack(a);
    std::cout << *me;
    me->attack(a);
    std::cout << *me;
    me->attack(a);
    std::cout << *me;
    me->recoverAP();
    me->recoverAP();
    me->recoverAP();
    me->equip(blam);
    std::cout << *me;
    me->recoverAP();
    std::cout << *me;
    me->recoverAP();
    std::cout << *me;
    me->recoverAP();
    me->attack(c);
    std::cout << *me;
    me->recoverAP();
    me->equip(bazooka);
    std::cout << *me;
    me->recoverAP();
    me->attack(c);
    std::cout << *me;
    me->recoverAP();
    delete me;
    delete pr;
    delete pf;
    delete a;
    delete b;
    return 0;
}