//
// Created by Francois-louis TALLEUX on 6/11/21.
//

#include "Enemy.hpp"

Enemy::Enemy(void) {
    this->HP = 0;
    this->type = "";
}

Enemy::Enemy(int hp, const std::string &types) {
    this->HP = hp;
    this->type = types;
}

Enemy::Enemy(const Enemy &src) {
    this->HP = src.getHP();
    this->type = src.getType();
}

Enemy::~Enemy() {}

Enemy &Enemy::operator=(const Enemy &src) {
    this->HP = src.getHP();
    this->type = src.getType();
    return (*this);
}

std::string Enemy::getType() const {
    std::string tmp = this->type;
    return (tmp);
}

int Enemy::getHP() const {
    int tmp = this->HP;
    return (tmp);
}

void Enemy::takeDamage(int amount) {
    if (amount >= this->HP)
        this->HP = 0;
    else if (amount > 0)
        this->HP -= amount;
}