//
// Created by Francois-louis TALLEUX on 6/12/21.
//

#ifndef CPP_MODULES_BLOP_HPP
#define CPP_MODULES_BLOP_HPP
#include "Enemy.hpp"

class Blop : public Enemy{
public:
    Blop(void);
    Blop(const Blop &);
    virtual ~Blop();
    void takeDamage(int amount);
    using Enemy::operator=;
};


#endif //CPP_MODULES_RADSCORPION_HPP