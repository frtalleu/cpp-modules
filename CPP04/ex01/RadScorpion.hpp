//
// Created by Francois-louis TALLEUX on 6/12/21.
//

#ifndef CPP_MODULES_RADSCORPION_HPP
#define CPP_MODULES_RADSCORPION_HPP
#include "Enemy.hpp"

class RadScorpion : public Enemy{
public:
    RadScorpion(void);
    RadScorpion(const RadScorpion &);
    virtual ~RadScorpion();
    void takeDamage(int amount);
    using Enemy::operator=;
};


#endif //CPP_MODULES_RADSCORPION_HPP