//
// Created by Francois-louis TALLEUX on 6/11/21.
//

#ifndef CPP_MODULES_ENEMY_HPP
#define CPP_MODULES_ENEMY_HPP
#include "string"
#include "iostream"

class Enemy {
protected:
    int HP;
    std::string type;
public:
    Enemy(void);
    Enemy(int hp, std::string const &types);
    Enemy(const Enemy &src);
    virtual ~Enemy();
    Enemy &operator=(const Enemy &src);
    int getHP() const;
    std::string getType() const;
    virtual void takeDamage(int amount);
};


#endif //CPP_MODULES_ENEMY_HPP