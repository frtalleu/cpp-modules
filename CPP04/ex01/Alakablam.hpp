//
// Created by Francois-louis TALLEUX on 6/8/21.
//

#ifndef CPP_MODULES_ALAKABLAM_HPP
#define CPP_MODULES_ALAKABLAM_HPP
#include "string"
#include "iostream"
#include "AWeapon.hpp"

class Alakablam : public AWeapon {
public:
    Alakablam(void);
    Alakablam(const Alakablam &src);
    virtual ~Alakablam();
    using AWeapon::operator=;
    void attack() const;
};


#endif //CPP_MODULES_PLASMARIFLE_HPP