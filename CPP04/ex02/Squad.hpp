//
// Created by Francois-louis TALLEUX on 6/13/21.
//

#ifndef CPP_MODULES_SQUAD_HPP
#define CPP_MODULES_SQUAD_HPP
#include "ISquad.hpp"
#include "ISpaceMarine.hpp"
#include "string"
#include "iostream"

typedef struct s_squad
{
    ISpaceMarine *marine;
    struct s_squad *next;
}   t_squad;

class Squad : public ISquad {
private:
    t_squad *squad;
public:
    int getCount() const;
    ISpaceMarine *getUnit(int pos) const;
    int push(ISpaceMarine *marines);
    Squad(void);
    Squad(const Squad &src);
    ~Squad();
    Squad &operator=(const Squad &src);
};


#endif //CPP_MODULES_SQUAD_HPP