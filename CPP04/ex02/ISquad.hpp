//
// Created by Francois-louis TALLEUX on 6/13/21.
//

#ifndef CPP_MODULES_ISQUAD_HPP
#define CPP_MODULES_ISQUAD_HPP
#include "ISpaceMarine.hpp"

class ISquad
{
public:
    virtual ~ISquad() {}
    virtual int getCount() const = 0;
    virtual ISpaceMarine* getUnit(int) const = 0;
    virtual int push(ISpaceMarine*) = 0;
};

#endif //CPP_MODULES_ISQUAD_HPP
