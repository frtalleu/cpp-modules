//
// Created by Francois-louis TALLEUX on 6/13/21.
//

#ifndef CPP_MODULES_ASSAULTTERMINATOR_HPP
#define CPP_MODULES_ASSAULTTERMINATOR_HPP
#include "ISpaceMarine.hpp"
#include "string"
#include "iostream"

class AssaultTerminator : public ISpaceMarine {
public:
    AssaultTerminator(void);
    AssaultTerminator(const AssaultTerminator &src);
    ~AssaultTerminator();
    AssaultTerminator &operator=(const AssaultTerminator &src);
    void battleCry() const;
    void rangedAttack() const;
    void meleeAttack() const;
    ISpaceMarine* clone() const;
};

#endif //CPP_MODULES_ASSAULTTERMINATOR_HPP
