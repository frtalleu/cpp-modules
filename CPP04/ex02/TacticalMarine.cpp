//
// Created by Francois-louis TALLEUX on 6/13/21.
//

#include "TacticalMarine.hpp"

TacticalMarine::TacticalMarine(void) {
    std::cout << "Tactical Marine ready for battle!" << std::endl;
}

TacticalMarine::TacticalMarine(const TacticalMarine &src) {
    (void)src;
    std::cout << "Tactical Marine ready for battle!" << std::endl;
}

TacticalMarine::~TacticalMarine() {
    std::cout << "Aaargh..." << std::endl;
}

void TacticalMarine::rangedAttack() const {
    std::cout <<  "* attacks with a bolter *" << std::endl;
}

void TacticalMarine::meleeAttack() const {
    std::cout << "* attacks with a chainsword *" << std::endl;
}

void TacticalMarine::battleCry() const {
    std::cout << "For the holy PLOT!" << std::endl;
}

ISpaceMarine *TacticalMarine::clone() const {
    return (new TacticalMarine(*this));
}

TacticalMarine &TacticalMarine::operator=(const TacticalMarine &src) {
    (void)src;
    return (*this);
}