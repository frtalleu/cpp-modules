//
// Created by Francois-louis TALLEUX on 6/13/21.
//

#ifndef CPP_MODULES_TACTICALMARINE_HPP
#define CPP_MODULES_TACTICALMARINE_HPP
#include "ISpaceMarine.hpp"
#include "string"
#include "iostream"

class TacticalMarine : public ISpaceMarine {
public:
    TacticalMarine(void);
    TacticalMarine(const TacticalMarine &src);
    ~TacticalMarine();
    TacticalMarine &operator=(const TacticalMarine &src);
    void battleCry() const;
    void rangedAttack() const;
    void meleeAttack() const;
    ISpaceMarine* clone() const;
};


#endif //CPP_MODULES_TACTICALMARINE_HPP