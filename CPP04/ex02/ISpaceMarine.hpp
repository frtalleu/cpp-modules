//
// Created by Francois-louis TALLEUX on 6/13/21.
//

#ifndef CPP_MODULES_ISPACEMARINE_HPP
#define CPP_MODULES_ISPACEMARINE_HPP


class ISpaceMarine
{
public:
    virtual ~ISpaceMarine() {}
    virtual ISpaceMarine* clone() const = 0;
    virtual void battleCry() const = 0;
    virtual void rangedAttack() const = 0;
    virtual void meleeAttack() const = 0;
};
#endif //CPP_MODULES_ISPACEMARINE_HPP
