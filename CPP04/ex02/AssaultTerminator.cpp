//
// Created by Francois-louis TALLEUX on 6/13/21.
//

#include "AssaultTerminator.hpp"

AssaultTerminator::AssaultTerminator(void) {
    std::cout << "* teleports from space *" << std::endl;
}

AssaultTerminator::AssaultTerminator(const AssaultTerminator &src) {
    (void)src;
    std::cout << "* teleports from space *" << std::endl;
}

AssaultTerminator::~AssaultTerminator() {
    std::cout << "I’ll be back..." << std::endl;
}

void AssaultTerminator::rangedAttack() const {
    std::cout << "* does nothing *" << std::endl;
}

void AssaultTerminator::meleeAttack() const {
    std::cout << "* attacks with a chainsfists *" << std::endl;
}

void AssaultTerminator::battleCry() const {
    std::cout << "This code is unclean. PURIFY IT!" << std::endl;
}

ISpaceMarine *AssaultTerminator::clone() const {
    return (new AssaultTerminator(*this));
}

AssaultTerminator &AssaultTerminator::operator=(const AssaultTerminator &src) {
    (void)src;
    return (*this);
}