//
// Created by Francois-louis TALLEUX on 6/13/21.
//

#include "Squad.hpp"

Squad::Squad(void) {
    this->squad = NULL;
}

Squad::Squad(const Squad &src) {
    int i = 0;
    this->squad = NULL;
    int j = src.getCount();

    while (i < j){
        this->push(src.getUnit(i));
        i++;
    }
}

Squad::~Squad() {
    t_squad *tmp = this->squad;
    t_squad *tmp2;
    while (tmp) {
        tmp2 = tmp;
        tmp = tmp->next;
        delete tmp2->marine;
        delete tmp2;
    }
}

int Squad::getCount() const {
    int i = 0;
    t_squad *tmp = this->squad;
    while (tmp != NULL){
        tmp = tmp->next;
        i++;
    }
    return (i);
}

ISpaceMarine *Squad::getUnit(int pos) const {
    int i = 0;
    t_squad *tmp = this->squad;
    while(tmp)
    {
        if(i == pos)
            return (tmp->marine);
        i++;
        tmp = tmp->next;
    }
    return (NULL);
}

int Squad::push(ISpaceMarine *marines) {
    t_squad *to_add = new t_squad ;
    to_add->marine = marines;
    to_add->next = NULL;
    int check = 0;
    if (this->squad == NULL) {
        this->squad = to_add;
        return(1);
    }
    else{
        t_squad *tmp = this->squad;
        int i = 1;
        if (&tmp->marine == &marines)
            check++;
        while (tmp->next != NULL){
            if (&tmp->marine == &marines)
                check++;
            tmp = tmp->next;
            i++;
        }
        if (marines == NULL || check != 0)
            return (i);
        tmp->next = to_add;
        return (i + 1);
    }
}

Squad &Squad::operator=(const Squad &src) {
    t_squad *tmp = this->squad;
    t_squad *tmp2;
    while (tmp){
        tmp2 = tmp;
        tmp = tmp->next;
        delete tmp2->marine;
        delete tmp2;
    }
    int i = 0;
    this->squad = NULL;
    int j = src.getCount();

    while (i < j){
        this->push(src.getUnit(i));
        i++;
    }
    return (*this);
}