//
// Created by Francois-louis TALLEUX on 6/14/21.
//

#include "Cure.hpp"

#include "Cure.hpp"

Cure::Cure(void) {
    this->type = "cure";
    this->_xp = 0;
}

Cure::Cure(const Cure &src) {
    this->type = src.getType();
    this->_xp = src.getXP();
}

Cure::~Cure() {}

AMateria *Cure::clone() const {
    return (new Cure(*this));
}

void Cure::use(ICharacter &target) {
    AMateria::use(target);
    std::cout << "* heals " << target.getName() << "’s wounds *" << std::endl;
}