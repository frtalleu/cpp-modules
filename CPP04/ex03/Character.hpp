//
// Created by Francois-louis TALLEUX on 6/14/21.
//

#ifndef CPP_MODULES_CHARACTER_HPP
#define CPP_MODULES_CHARACTER_HPP
#include "ICharacter.hpp"
#include "AMateria.hpp"
#include "string"

class Character : public ICharacter{
private:
    std::string name;
    AMateria *inventory[4];
public:
    Character(void);
    Character(const Character &src);
    Character(std::string name);
    ~Character();
    AMateria *get_inventory() const;
    std::string const &getName() const;
    void equip(AMateria *m);
    void unequip(int idx);
    void use(int idx, ICharacter &target);
    Character &operator=(const Character &src);
};


#endif //CPP_MODULES_CHARACTER_HPP