//
// Created by Francois-louis TALLEUX on 6/14/21.
//

#ifndef CPP_MODULES_CURE_HPP
#define CPP_MODULES_CURE_HPP
#include "IMateriaSource.hpp"
#include "AMateria.hpp"
#include "ICharacter.hpp"

class Cure : public AMateria{
public:
    Cure(void);
    Cure(const Cure &src);
    virtual  ~Cure();
    virtual void use(ICharacter& target);
    AMateria *clone() const;
    using AMateria::operator=;
};


#endif //CPP_MODULES_CURE_HPP