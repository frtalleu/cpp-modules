//
// Created by Francois-louis TALLEUX on 6/14/21.
//

#ifndef CPP_MODULES_IMATERIASOURCE_H
#define CPP_MODULES_IMATERIASOURCE_H
#include "AMateria.hpp"

class IMateriaSource {
public:
    virtual ~IMateriaSource() {}
    virtual void learnMateria(AMateria*) = 0;
    virtual AMateria* createMateria(std::string const & type) = 0;
};

#endif //CPP_MODULES_IMATERIASOURCE_H
