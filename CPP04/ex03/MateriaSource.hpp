//
// Created by Francois-louis TALLEUX on 6/14/21.
//

#ifndef CPP_MODULES_MATERIASOURCE_HPP
#define CPP_MODULES_MATERIASOURCE_HPP
#include "IMateriaSource.hpp"
#include "AMateria.hpp"

class MateriaSource :public IMateriaSource{
private:
    AMateria *inventory[4];
public:
    MateriaSource(void);
    MateriaSource(const MateriaSource &src);
    virtual ~MateriaSource();
    AMateria *get_inventory() const;
    MateriaSource &operator=(const MateriaSource &src);
    void learnMateria(AMateria *m);
    AMateria *createMateria(std::string const &type);
};


#endif //CPP_MODULES_MATERIASOURCE_HPP