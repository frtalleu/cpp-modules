//
// Created by Francois-louis TALLEUX on 6/14/21.
//

#ifndef CPP_MODULES_ICE_HPP
#define CPP_MODULES_ICE_HPP
#include "IMateriaSource.hpp"
#include "AMateria.hpp"
#include "ICharacter.hpp"

class Ice : public AMateria{
public:
    Ice(void);
    Ice(const Ice &src);
    virtual  ~Ice();
    virtual void use(ICharacter& target);
    AMateria *clone() const;
    using AMateria::operator=;
};

#endif //CPP_MODULES_ICE_HPP