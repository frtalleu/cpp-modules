//
// Created by Francois-louis TALLEUX on 6/14/21.
//

#include "AMateria.hpp"

AMateria::AMateria(void) {
    this->type = "";
    this->_xp = 0;
}

AMateria::AMateria(const AMateria &src) {
    this->_xp = src.getXP();
    this->type = src.getType();
}

AMateria::AMateria(const std::string &type) {
    this->type = type;
    this->_xp = 0;
}

AMateria::~AMateria() {}

void AMateria::use(ICharacter &target) {
    this->_xp += 10;
    (void)target;
}

unsigned int AMateria::getXP() const {
    return (this->_xp);
}

std::string const &AMateria::getType() const {
    return (this->type);
}

AMateria &AMateria::operator=(const AMateria &src) {
    this->_xp = src.getXP();
    this->type = src.getType();
    return (*this);
}