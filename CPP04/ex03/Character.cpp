//
// Created by Francois-louis TALLEUX on 6/14/21.
//

#include "Character.hpp"

Character::Character(void) {
    this->name = "Default";
    this->inventory[0] = NULL;
    this->inventory[1] = NULL;
    this->inventory[2] = NULL;
    this->inventory[3] = NULL;
}

Character::Character(std::string name) {
    this->name = name;
    this->inventory[0] = NULL;
    this->inventory[1] = NULL;
    this->inventory[2] = NULL;
    this->inventory[3] = NULL;
}

Character::Character(const Character &src) {
    int i = 0;
    AMateria *tmp = src.get_inventory();
    while (i < 4){
        if (&tmp[i] != NULL)
            this->inventory[i] = tmp[i].clone();
        else
            this->inventory[i] = NULL;
        i++;
    }
    this->name = src.getName();
}

void Character::equip(AMateria *m) {
    int i = 0;
    while (i < 4){
        if (this->inventory[i] == NULL){
            this->inventory[i] = m;
            return;
        }
        i++;
    }
}

void Character::unequip(int idx) {
    if (idx >= 0 && idx < 4)
        this->inventory[idx] = NULL;
}

void Character::use(int idx, ICharacter &target) {
    if (this->inventory[idx] != NULL){
        this->inventory[idx]->use(target);
    }
}

Character::~Character() {
    int i = 0;
    while (i < 4){
        if (this->inventory[i] != NULL)
            delete this->inventory[i];
        i++;
    }
}

AMateria *Character::get_inventory() const {
    return(*(this->inventory));
}

std::string const &Character::getName() const {
    return (this->name);
}

Character &Character::operator=(const Character &src) {
    AMateria *tmp = src.get_inventory();
    int i = 0;
    while (i < 4){
        if (this->inventory[i] != NULL)
            delete this->inventory[i];
        i++;
    }
    i = 0;
    while (i < 4){
        if (&tmp[i] != NULL)
            this->inventory[i] = tmp[i].clone();
        else
            this->inventory[i] = NULL;
        i++;
    }
    this->name = src.getName();
    return (*this);
}