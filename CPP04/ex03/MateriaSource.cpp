//
// Created by Francois-louis TALLEUX on 6/14/21.
//

#include "MateriaSource.hpp"

MateriaSource::MateriaSource(void) {
    this->inventory[0] = NULL;
    this->inventory[1] = NULL;
    this->inventory[2] = NULL;
    this->inventory[3] = NULL;
}

MateriaSource::MateriaSource(const MateriaSource &src) {
    int i = 0;
    AMateria *tmp = src.get_inventory();
    while (i < 4){
        if (&tmp[i] != NULL)
            this->inventory[i] = tmp[i].clone();
        else
            this->inventory[i] = NULL;
        i++;
    }
}

MateriaSource::~MateriaSource() {
    int i = 0;
    while (i < 4){
        if (this->inventory[i] != NULL)
            delete this->inventory[i];
        i++;
    }
}

AMateria *MateriaSource::get_inventory() const {
    return (*(this->inventory));
}

MateriaSource &MateriaSource::operator=(const MateriaSource &src) {
    int i = 0;
    AMateria *tmp = src.get_inventory();
    while (i < 4){
        if (&tmp[i] != NULL)
            this->inventory[i] = tmp[i].clone();
        else
            this->inventory[i] = NULL;
        i++;
    }
    return (*this);
}

void MateriaSource::learnMateria(AMateria *m) {
    int i = 0;
    while(i < 4){
        if (this->inventory[i] == NULL) {
            this->inventory[i] = m;
            return;
        }
        i++;
    }
}

AMateria *MateriaSource::createMateria(const std::string &type) {
    int i = 0;
    AMateria *tmp = NULL;
    while(i < 4){
        if (type.compare(this->inventory[i]->getType()) == 0){
            tmp = this->inventory[i]->clone();
            return (tmp);
        }
        i++;
    }
    return (tmp);
}