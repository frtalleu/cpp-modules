//
// Created by Francois-louis TALLEUX on 6/14/21.
//

#ifndef CPP_MODULES_AMATERIA_HPP
#define CPP_MODULES_AMATERIA_HPP
#include "string"
#include "iostream"
#include "ICharacter.hpp"

class ICharacter;
class AMateria
{
protected:
    std::string type;
    unsigned int _xp;
public:
    AMateria(std::string const & type);
    AMateria(const AMateria &src);
    AMateria(void);
    virtual ~AMateria();
    std::string const & getType() const; //Returns the materia type
    unsigned int getXP() const; //Returns the Materia's XP
    virtual AMateria* clone() const = 0;
    virtual void use(ICharacter& target);
    AMateria &operator=(const AMateria &src);
};


#endif //CPP_MODULES_AMATERIA_HPP