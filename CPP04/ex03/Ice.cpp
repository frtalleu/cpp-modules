//
// Created by Francois-louis TALLEUX on 6/14/21.
//

#include "Ice.hpp"

Ice::Ice(void) {
    this->type = "ice";
    this->_xp = 0;
}

Ice::Ice(const Ice &src) {
    this->type = src.getType();
    this->_xp = src.getXP();
}

Ice::~Ice() {}

AMateria *Ice::clone() const {
    return (new Ice(*this));
}

void Ice::use(ICharacter &target) {
    AMateria::use(target);
    std::cout << "* shoots an ice bolt at " << target.getName() << " *" << std::endl;
}