//
// Created by François-Louis Talleux on 07/05/2021.
//

#ifndef CPP_FIXED_HPP
#define CPP_FIXED_HPP
#include "iostream"
#include "string"

class Fixed {
    public:
        Fixed(void);
        Fixed(const Fixed &src);
        Fixed(const int i);
        Fixed(const float f);
        ~Fixed(void);
        Fixed &operator=(const Fixed &rhs);
        int getRawBits(void) const;
        void setRawBits(int const raw);
        float toFloat(void) const;
        int toInt(void) const;
    private:
        int fixed_point;
        static const int nb_fract_bits = 8;

};

std::ostream &operator<<(std::ostream &os, const Fixed &rhs);

#endif //CPP_FIXED_HPP
