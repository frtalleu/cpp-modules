//
// Created by François-Louis Talleux on 07/05/2021.
//

#ifndef CPP_FIXED_HPP
#define CPP_FIXED_HPP


class Fixed {
    public:
        Fixed(void);
        Fixed(const Fixed &src);
        ~Fixed(void);
        Fixed &operator=(const Fixed &rhs);
        int getRawBits(void) const;
        void setRawBits(int const raw);
    private:
        int fixed_point;
        static const int nb_fract_bits = 8;

};


#endif //CPP_FIXED_HPP
