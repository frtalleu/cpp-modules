//
// Created by Francois-louis TALLEUX on 7/6/21.
//

#include "string"
#include "iostream"
#include "cstdlib"
#include "exception"
#include "typeinfo"

class Base{
public:
    virtual ~Base(){};
};

class A : public Base{
public:
    A(){};
};
class B : public Base{
public:
    B(){};
};
class C : public Base{
public:
    C(){};
};

Base *generate(void){
    srand(time(0));
    int i = std::rand() % 3;
    if (i == 0) {
        std::cout << "This is A" << std::endl;
        return (new A);
    }
    else if (i == 1){
        std::cout << "This is B" << std::endl;
        return (new B);
    }
    else if (i == 2){
        std::cout << "This is C" << std::endl;
        return (new C);
    }
    return (NULL);
}

void identify(Base* p){
    try{
        A &a= dynamic_cast<A &>(*p);
        std::cout << "A" << std::endl;
        (void)a;
    }
    catch(std::bad_cast &a){
        try{
            B &b= dynamic_cast<B &>(*p);
            std::cout << "B" << std::endl;
            (void)b;
        }
        catch(std::bad_cast &a){
            try{
                C &c= dynamic_cast<C &>(*p);
                std::cout << "C" << std::endl;
                (void)c;
            }
            catch(std::bad_cast &a){
                std::cout << "rien" << std::endl;
            }
        }

    }
}


void identify(Base& p){
    try{
        A &a= dynamic_cast<A &>(p);
        std::cout << "A" << std::endl;
        (void)a;
    }
    catch(std::bad_cast &c){
        try{
            B &b= dynamic_cast<B &>(p);
            std::cout << "B" << std::endl;
            (void)b;
        }
        catch(std::exception &a){
            try{
                C &c= dynamic_cast<C &>(p);
                std::cout << "C" << std::endl;
                (void)c;
            }
            catch(std::exception &a){
                std::cout << "rien" << std::endl;
            }
        }
    }
}

int main()
{
    Base *base = generate();
  //  Base &base1 = base;
    identify(base);
    identify(*base);

}
