//
// Created by Francois-louis TALLEUX on 7/6/21.
//

//#include "Scalar.hpp"
#include "string"
#include "iostream"
#include "math.h"

int str_is_nbr(char *str){
    int i  = 0;
    while (str[i]){
        if ((str[i] == '-' && i == 0) || (str[i] >= 48 && str[i] <=57) || (str[i] == 'f' && str[i + 1] == '\0' && i != 0) || (str[i] == '.' && str[i + 1] >= 48 && str[i + 1] <=57))
            i++;
        else
            return (0);
    }
    return (1);
}

int main(int ac, char **av){
    if (ac != 2) {
        std::cout << "Must have 2 arguments" << std::endl;
        return (0);
    }
    std::string tmp;
    std::string tmp1;
    std::string value(av[1]);
    float stock;

    if (value != "nan" && value != "nanf" && value != "+inf" && value != "-inf" && value != "+inff" && value != "-inff" && str_is_nbr(av[1]) == 0 && value.length() != 1)
    {
        std::cout << "char: impossible" << std::endl;
        std::cout << "int: impossible" << std::endl;
        std::cout << "float: impossible" << std::endl;
        std::cout << "double: impossible" << std::endl;
        return (0);
    }
    if (value.length() == 1 && (av[1][0] < 48 || av[1][0] > 57))
        stock = static_cast<float>(av[1][0]);
    else
        stock = atof(av[1]);
    //manage char
    char c = static_cast<char>(stock);
    if (stock == atof("inf") || stock == atof("-inf") || stock != stock || stock < -128.0f || stock > 127.0f)
        std::cout << "char: impossible" << std::endl;
    else if (stock < 32.0f || stock > 126.0f)
        std::cout << "char: Non displayable" << std::endl;
    else
        std::cout << "char: " << "'" << c << "'" << std::endl;
    //manage int
    int i = atoi(value.c_str());
    if (value.length() == 1)
        i = static_cast<int>(stock);
    if (value.length() != 1 && (stock == atof("inf") || stock == atof("-inf") || stock != stock || (av[1][0] == '-' && i > 0) || (av[1][0] != '-' && i < 0)))
        std::cout << "int: impossible" << std::endl;
    else
        std::cout << "int: " << i << std::endl;
    //manage float
    if (stock == atof("inf"))
        tmp = "+";
    else
        tmp = "";
    if (stock == static_cast<float>(i))
        tmp1 = ".0";
    else
        tmp1 = "";
    std::cout << "float: " << tmp << stock  << tmp1 << "f" << std::endl;
    //manage double
    double d = static_cast<double>(stock);
    std::cout << "double: " << tmp << d << tmp1 << std::endl;
}