//
// Created by Francois-louis TALLEUX on 7/6/21.
//
#include "string"
#include "iostream"
#include "stdint.h"

class Data{
public:
    Data(){};
    ~Data(){};
private:
    Data(const Data &src){(void)src;}
    const Data &operator=(const Data &rhs){return (rhs);}
};

uintptr_t serialize(Data* ptr){
    return (reinterpret_cast<uintptr_t>(ptr));
}

Data* deserialize(uintptr_t raw){
    return (reinterpret_cast<Data *>(raw));
}

int main()
{
    Data *data = new Data();
    uintptr_t ptr =  serialize(data);;
    Data *data1 = deserialize(ptr);
    std::cout << "0x" << std::hex << ptr << std::endl;
    std::cout << data1 << std::endl;
    delete data;
}
