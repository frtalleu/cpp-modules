//
// Created by Francois-louis TALLEUX on 6/22/21.
//

#include "ShrubberyCreationForm.hpp"
#include "fstream"
#include "iostream"

ShrubberyCreationForm::ShrubberyCreationForm(std::string targets) : Form("Shurberry Creation Form", 145, 137) {
    this->target = targets;
}

ShrubberyCreationForm::ShrubberyCreationForm(void) : Form("Shurberry Creation Form", 145, 137){
    this->target = "default";
}

ShrubberyCreationForm::ShrubberyCreationForm(const ShrubberyCreationForm &src) : Form(src), target(src.getTarget()) {}

ShrubberyCreationForm::~ShrubberyCreationForm() {}

ShrubberyCreationForm &ShrubberyCreationForm::operator=(const ShrubberyCreationForm &src) {
    this->target = src.getTarget();
    return (*this);
}

void ShrubberyCreationForm::execute(const Bureaucrat &executor) const {
    std::string tmp;
    if (executor.getGrade() > this->getGrade_exec())
        throw Form::GradeTooLowException();
    tmp = target + "_shrubbery";
    std::ofstream ifs;
    ifs.open(tmp.c_str());
    ifs << "    " << std::endl;
    ifs << "   / \\" << std::endl;
    ifs << "  /  \\" << std::endl;
    ifs << " /   \\" << std::endl;
    ifs << "/     \\" << std::endl;
    ifs << "   ||" << std::endl;
    ifs << "   ||" << std::endl;
    ifs.close();

}

std::string ShrubberyCreationForm::getTarget() const {
    return (this->target);
}
