//
// Created by Francois-louis TALLEUX on 6/24/21.
//

#ifndef CPP_MODULES_INTERN_HPP
#define CPP_MODULES_INTERN_HPP
#include "Form.hpp"
#include "ShrubberyCreationForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"

class Intern {
public:
    Intern(void);
    Intern(const Intern &src);
    ~Intern();
    Intern &operator=(const Intern &src);
    Form *makeForm(std::string form_name, std::string target);
};


#endif //CPP_MODULES_INTER_HPP