//
// Created by Francois-louis TALLEUX on 6/22/21.
//

#ifndef CPP_MODULES_SHRUBBERYCREATIONFORM_HPP
#define CPP_MODULES_SHRUBBERYCREATIONFORM_HPP
#include "Form.hpp"
#include "Bureaucrat.hpp"

class ShrubberyCreationForm : public Form {
private:
    std::string target;
public:
    ShrubberyCreationForm(void);
    ShrubberyCreationForm(const ShrubberyCreationForm &src);
    ShrubberyCreationForm(std::string targets);
    ~ShrubberyCreationForm();
    std::string getTarget() const;
    ShrubberyCreationForm &operator=(const ShrubberyCreationForm &src);
    void execute (Bureaucrat const &executor) const;
};


#endif //CPP_MODULES_SHRUBBERYCREATIONFORM_HPP