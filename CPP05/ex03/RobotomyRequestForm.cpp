//
// Created by Francois-louis TALLEUX on 6/22/21.
//

#include "RobotomyRequestForm.hpp"
#include "fstream"
#include "cstdlib"

RobotomyRequestForm::RobotomyRequestForm(std::string targets) : Form("Robotomy Request Form", 72, 45) {
    this->target = targets;
}

RobotomyRequestForm::RobotomyRequestForm(void) : Form("Robotomy Request Form", 72, 45){
    this->target = "default";
}

RobotomyRequestForm::RobotomyRequestForm(const RobotomyRequestForm &src) : Form(src), target(src.getTarget()) {
}

RobotomyRequestForm::~RobotomyRequestForm() {}

RobotomyRequestForm &RobotomyRequestForm::operator=(const RobotomyRequestForm &src) {
    this->target = src.getTarget();
    return (*this);
}

void Bureaucrat::signForm(Form &form) const {
    if (this->grade > form.getGrade_sign())
        std::cout << this->name << " can't sign " << form.getName() << " because the grade is too low." << std::endl;
    else if (form.getSign())
        std::cout << this->name << " can't sign " << form.getName() << " because the form is already signed." << std::endl;
    else
        std::cout << this->name << " signs " << form.getName() << std::endl;
    form.beSigned(*this);
}

void RobotomyRequestForm::execute(const Bureaucrat &executor) const {
    srand(time(0));
    std::string tmp;
    if (executor.getGrade() > this->getGrade_exec())
        throw Form::GradeTooLowException();
    int i = std::rand() % 2;
    if (i == 1)
        tmp = " is robotomized";
    else
        tmp = " is not robotomized";
    std::cout << this->target <<  tmp << std::endl;
}

std::string RobotomyRequestForm::getTarget() const {
    return (this->target);
}
