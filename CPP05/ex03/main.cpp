//
// Created by Francois-louis TALLEUX on 6/18/21.
//

#include "Bureaucrat.hpp"
#include "string"
#include "iostream"
#include "stdexcept"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "Intern.hpp"

int main(){
    Intern inter;
    Form *shrubbery = inter.makeForm("shrubbery creation", "rien");
    Form *presidential = inter.makeForm("presidential pardon", "everybody");
    Form *robotomy = inter.makeForm("robotomy request", "The intern");
    std::cout << *shrubbery << *presidential << *robotomy;
    Bureaucrat big_boss("BIG BOSS", 1);
    big_boss.signForm(*shrubbery);
    big_boss.signForm(*presidential);
    big_boss.signForm(*robotomy);
    big_boss.executeForm(*shrubbery);
    big_boss.executeForm(*presidential);
    big_boss.executeForm(*robotomy);
    delete shrubbery;
    delete presidential;
    delete robotomy;
}

