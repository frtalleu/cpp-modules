//
// Created by Francois-louis TALLEUX on 6/22/21.
//

#include "PresidentialPardonForm.hpp"
#include "fstream"

PresidentialPardonForm::PresidentialPardonForm(std::string targets) : Form("Presidential Pardon Form", 25, 5) {
    this->target = targets;
}

PresidentialPardonForm::PresidentialPardonForm(void) : Form("Presidential Pardon Form", 25, 5){
    this->target = "default";
}

PresidentialPardonForm::PresidentialPardonForm(const PresidentialPardonForm &src) : Form(src), target(src.getTarget()){
}

PresidentialPardonForm::~PresidentialPardonForm() {}

PresidentialPardonForm &PresidentialPardonForm::operator=(const PresidentialPardonForm &src) {
    this->target = src.getTarget();
    return (*this);
}

void PresidentialPardonForm::execute(const Bureaucrat &executor) const {
    if (executor.getGrade() > this->getGrade_exec())
        throw Form::GradeTooLowException();
    std::cout << this->target << " has been pardoned by Zafod Beeblebrox." << std::endl;
}

std::string PresidentialPardonForm::getTarget() const {
    return (this->target);
}