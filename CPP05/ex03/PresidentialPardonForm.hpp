//
// Created by Francois-louis TALLEUX on 6/22/21.
//

#ifndef CPP_MODULES_PRESIDENTIALPARDONFORM_HPP
#define CPP_MODULES_PRESIDENTIALPARDONFORM_HPP
#include "Form.hpp"
#include "Bureaucrat.hpp"

class PresidentialPardonForm : public Form {
private:
    std::string target;
public:
    PresidentialPardonForm(void);
    PresidentialPardonForm(const PresidentialPardonForm &src);
    PresidentialPardonForm(std::string targets);
    ~PresidentialPardonForm();
    std::string getTarget() const;
    PresidentialPardonForm &operator=(const PresidentialPardonForm &src);
    void execute (Bureaucrat const &executor) const;
};


#endif