//
// Created by Francois-louis TALLEUX on 6/24/21.
//

#include "Intern.hpp"

static Form *create_robotomy_resquest(std::string target) {return (new RobotomyRequestForm(target));}
static Form *create_shrubberry_creation(std::string target) {return (new ShrubberyCreationForm(target));}
static Form *create_presidential_pardon(std::string target) {return (new PresidentialPardonForm(target));}

Intern::Intern(void) {}
Intern::Intern(const Intern &src) {(void)src;}
Intern::~Intern() {}
Intern &Intern::operator=(const Intern &src) {(void)src; return(*this);}

Form * Intern::makeForm(std::string form_name, std::string target) {
    typedef struct s_type{ std::string form_type; Form* (*f)(std::string target);} t_type;
    t_type lst_form[3] = {
            {"robotomy request", &create_robotomy_resquest},
            {"shrubbery creation", &create_shrubberry_creation},
            {"presidential pardon", &create_presidential_pardon}
    };
    int i = 0;
    while (i < 3){
        if (form_name == lst_form[i].form_type)
            return (lst_form[i].f(target));
        i++;
    }
    return (NULL);
}