//
// Created by Francois-louis TALLEUX on 6/16/21.
//

#ifndef CPP_MODULES_BUREAUCRAT_HPP
#define CPP_MODULES_BUREAUCRAT_HPP

#include "string"
#include "iostream"
#include "stdexcept"

class Bureaucrat {
private:
    const std::string name;
    int grade;
public:
    Bureaucrat(void);
    Bureaucrat(std::string names, int grades);
    Bureaucrat(const Bureaucrat &src);
    ~Bureaucrat();
    Bureaucrat &operator=(const Bureaucrat &src);
    std::string getName() const;
    int getGrade() const;
    void up_grade();
    void down_grade();
    class GradeTooHighException : public std::exception {
    public:
        const char * what() const throw();
    };
    class GradeTooLowException : public std::exception {
    public:
        const char * what() const throw();
    };
};

std::ostream &operator<<(std::ostream &os, const Bureaucrat &rhs);

#endif //CPP_MODULES_BUREAUCRAT_HPP