//
// Created by Francois-louis TALLEUX on 6/18/21.
//

#include "Bureaucrat.hpp"
#include "string"
#include "iostream"
#include "stdexcept"

int main(){
    try {
        Bureaucrat moi("MOI", 0);
    }
    catch (std::exception &e) {
        std::cout << e.what() << std::endl;
    }
    try {
        Bureaucrat FDB("TOI", 151);
    }
    catch (std::exception &a) {
        std::cout << a.what() << std::endl;
    }
    try {
        Bureaucrat rien("lui", 150);
        std::cout << rien;
        rien.down_grade();
    }
    catch (std::exception &a) {
        std::cout << a.what() << std::endl;
    }
    try {
        Bureaucrat rie("elle", 1);
        std::cout << rie;
        rie.up_grade();
    }
    catch (std::exception &a) {
        std::cout << a.what() << std::endl;
    }
}

