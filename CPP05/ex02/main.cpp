//
// Created by Francois-louis TALLEUX on 6/18/21.
//

#include "Bureaucrat.hpp"
#include "string"
#include "iostream"
#include "stdexcept"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"

int main(){
    try {
        Bureaucrat ok("Someone", 50);
        ShrubberyCreationForm rien("ok");
        RobotomyRequestForm robot("Romaing");
        PresidentialPardonForm president("FL");
        ok.signForm(rien);
        ok.signForm(robot);
        ok.signForm(president);
        ok.executeForm(rien);
        ok.executeForm(robot);
        ok.executeForm(president);

    }
    catch (std::exception &a)
    {
        std::cout << a.what() << std::endl;
    }
    try {
        Bureaucrat ok("Someone", 5);
        ShrubberyCreationForm rien("ok");
        RobotomyRequestForm robot("Romaing");
        PresidentialPardonForm president("FL");
        ok.signForm(rien);
        ok.signForm(robot);
        ok.signForm(president);
        ok.executeForm(rien);
        ok.executeForm(robot);
        ok.executeForm(president);
    }
    catch (std::exception &a)
    {
        std::cout << a.what() << std::endl;
    }
}

