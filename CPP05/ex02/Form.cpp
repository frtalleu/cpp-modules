//
// Created by Francois-louis TALLEUX on 6/19/21.
//

#include "Form.hpp"

Form::Form(void) : name("DEFAULT"), grade_sign(150), grade_exec(150) {
    this->signe = false;
}

Form::Form(const Form &src): name(src.getName()), grade_sign(src.getGrade_sign()), grade_exec(src.getGrade_exec()){
    this->signe = getSign();
}

Form::Form(std::string names, int grade_to_sign, int grade_to_exec) : name(names), grade_sign(grade_to_sign), grade_exec(grade_to_exec) {
    if (grade_to_sign < 1 || grade_to_exec < 1)
        throw Form::GradeTooHighException();
    else if (grade_to_sign > 150 || grade_to_exec > 150)
        throw Form::GradeTooLowException();
    this->signe = false;
}

Form::~Form() {}

std::string Form::getName() const {
    return (this->name);
}

int Form::getGrade_sign() const {
    return (this->grade_sign);
}

bool Form::getSign() const {
    return (this->signe);
}

int Form::getGrade_exec() const {
    return (this->grade_exec);
}

Form &Form::operator=(const Form &src) {
    this->signe = src.getSign();
    return (*this);
}

void Form::beSigned(Bureaucrat const &bureaucrat) {
    if (bureaucrat.getGrade() > this->getGrade_sign())
        throw Form::GradeTooLowException();
    else
        this->signe = true;
}

const char * Form::GradeTooHighException::what() const throw() {return ("Grade is too high!");}

const char * Form::GradeTooLowException::what() const throw() {return ("Grade is too low!");}

std::ostream &operator<<(std::ostream &os, const Form &src){
    os << "this form " << src.getName() << ", this form need a grade of " << src.getGrade_sign() << " to be signed and a grade of " << src.getGrade_exec() << " to be executed" << std::endl;
    return (os);
}
