//
// Created by Francois-louis TALLEUX on 6/16/21.
//

#include "Bureaucrat.hpp"

Bureaucrat::Bureaucrat(void) : name("Default"){
    this->grade = 150;
}

Bureaucrat::Bureaucrat(const Bureaucrat &src) : name(src.getName()){
    if (src.getGrade() < 1)
        throw Bureaucrat::GradeTooHighException();
    else if (src.getGrade() > 150)
        throw Bureaucrat::GradeTooLowException();
    this->grade = src.getGrade();
}

Bureaucrat::Bureaucrat(std::string names, int grades) : name(names) {
    if (grades < 1)
        throw Bureaucrat::GradeTooHighException();
    else if(grades > 150)
        throw Bureaucrat::GradeTooLowException();
    this->grade = grades;
}

void Bureaucrat::up_grade() {
    if (this->grade == 1)
        throw Bureaucrat::GradeTooHighException();
    this->grade--;
}

void Bureaucrat::down_grade() {
    if (this->grade == 150)
        throw Bureaucrat::GradeTooLowException();
    this->grade++;
}

std::string Bureaucrat::getName() const {return (this->name);}

int Bureaucrat::getGrade() const {return (this->grade);}

Bureaucrat::~Bureaucrat() {}

Bureaucrat &Bureaucrat::operator=(const Bureaucrat &src) {
    if (grade < 1)
        throw Bureaucrat::GradeTooHighException();
    else if(grade > 150)
        throw Bureaucrat::GradeTooLowException();
    this->grade = src.getGrade();
    return (*this);
}

const char * Bureaucrat::GradeTooHighException::what() const throw() {return ("Grade is too high!");}

const char * Bureaucrat::GradeTooLowException::what() const throw() {return ("Grade is too low!");}

void Bureaucrat::executeForm(const Form &form) const {
    if (form.getSign() == false)
        std::cout << this->name << " can't executs " << form.getName() << " because the form is not signed" << std::endl;
    else if (this->grade > form.getGrade_exec())
        std::cout << this->name << " can't executs " << form.getName() << " because his grade is too low" << std::endl;
    else
        std::cout << this->name << " executs " << form.getName() << std::endl;
    form.execute(*this);
}

std::ostream &operator<<(std::ostream &os, const Bureaucrat &rhs){
    os << rhs.getName() << ", bureaucrat grade " << rhs.getGrade() << std::endl;
    return (os);
}