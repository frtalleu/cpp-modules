//
// Created by Francois-louis TALLEUX on 6/22/21.
//
#ifndef CPP_MODULES_ROBOTOMYREQUESTFORM_HPP
#define CPP_MODULES_ROBOTOMYREQUESTFORM_HPP
#include "Form.hpp"
#include "Bureaucrat.hpp"

class RobotomyRequestForm : public Form {
private:
    std::string target;
public:
    RobotomyRequestForm(void);
    RobotomyRequestForm(const RobotomyRequestForm &src);
    RobotomyRequestForm(std::string targets);
    ~RobotomyRequestForm();
    std::string getTarget() const;
    RobotomyRequestForm &operator=(const RobotomyRequestForm &src);
    void execute (Bureaucrat const &executor) const;
};


#endif //CPP_MODULES_SHRUBBERYCREATIONFORM_HPP