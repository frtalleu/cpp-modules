//
// Created by Francois-louis TALLEUX on 6/19/21.
//

#ifndef CPP_MODULES_FORM_HPP
#define CPP_MODULES_FORM_HPP

#include "string"
#include "Bureaucrat.hpp"
#include "iostream"
#include "stdexcept"

class Bureaucrat;

class Form {
public:
    Form(void);
    Form(const Form &src);
    Form(std::string names, int grade_to_sign, int grade_to_exec);
    virtual ~Form();
    Form &operator=(const Form &src);
    bool getSign() const;
    std::string getName() const;
    int getGrade_sign() const;
    int getGrade_exec() const;
    void beSigned(Bureaucrat const &bureaucrat);
    class GradeTooHighException : public std::exception {
    public:
        const char * what() const throw();
    };
    class GradeTooLowException : public std::exception {
    public:
        const char * what() const throw();
    };

private:
    bool signe;
    const std::string name;
    const int grade_sign;
    const int grade_exec;
};

std::ostream &operator<<(std::ostream &os, const Form &src);

#endif //CPP_MODULES_FORM_HPP
