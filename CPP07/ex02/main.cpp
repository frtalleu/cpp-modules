//
// Created by Francois-louis TALLEUX on 7/8/21.
//

#include <iostream>
#include <string>
#include "Array.hpp"

int main(void)
{
    std::cout << "=====TEST FOR INT=====" << std::endl;
    Array<int> array_int(5);
    for (unsigned int i = 0; i < array_int.size(); i++) {
        try{
            array_int[i] = i;
        }
        catch (std::exception &a) {
            std::cout << a.what() << std::endl;
        }
    }
    try{
        array_int[6] = 6;
    }
    catch (std::exception &a) {
        std::cout << a.what() << std::endl;
    }

    try{
        array_int[-2] = 6;
    }
    catch (std::exception &a) {
        std::cout << a.what() << std::endl;
    }

    for(unsigned int i = 0; i < array_int.size(); i++){
        try {
            std::cout << array_int[i] << " ";
        }
        catch (std::exception &a) {
            std::cout << a.what() << std::endl;
        }
    }
    std::cout << std::endl;

    std::cout << "====TEST FOR FLOAT====" << std::endl;
    Array<float> array_float(5);
    for (unsigned int i = 0; i < array_float.size(); i++) {
        try{
            array_float[i] = 9.54;
        }
        catch (std::exception &a) {
            std::cout << a.what() << std::endl;
        }
    }
    try{
        array_float[6] = 6.2;
    }
    catch (std::exception &a) {
        std::cout << a.what() << std::endl;
    }

    try{
        array_float[-2] = 6.2;
    }
    catch (std::exception &a) {
        std::cout << a.what() << std::endl;
    }

    for(unsigned int i = 0; i < array_float.size(); i++){
        try {
            std::cout << array_float[i] << " ";
        }
        catch (std::exception &a) {
            std::cout << a.what() << std::endl;
        }
    }
    std::cout << std::endl;

    std::cout << "====TEST FOR STRING===" << std::endl;
    Array<std::string> array_string(5);
    for (unsigned int i = 0; i < array_string.size(); i++) {
        try{
            array_string[i] = "bonjour";
        }
        catch (std::exception &a) {
            std::cout << a.what() << std::endl;
        }
    }
    try{
        array_string[6] = "ok";
    }
    catch (std::exception &a) {
        std::cout << a.what() << std::endl;
    }

    try{
        array_string[-2] = "ok";
    }
    catch (std::exception &a) {
        std::cout << a.what() << std::endl;
    }

    for(unsigned int i = 0; i < array_string.size(); i++){
        try {
            std::cout << array_string[i] << " ";
        }
        catch (std::exception &a) {
            std::cout << a.what() << std::endl;
        }
    }
    std::cout << std::endl;
}
