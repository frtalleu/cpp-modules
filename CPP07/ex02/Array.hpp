//
// Created by Francois-louis TALLEUX on 7/8/21.
//

#ifndef CPP_MODULES_ARRAY_HPP
#define CPP_MODULES_ARRAY_HPP
#include "string"
#include "iostream"

template<typename T>
class Array{
public:
    Array<T>(void) {this->s = 0; this->elem = NULL;};

    Array<T>(unsigned int size){
        this->s = size;
        this->elem = NULL;
        if (this->s != 0)
            this->elem = new T[s]();
    };

    Array<T>(Array<T> const &array){
        this->s = array.size();
        if (this->s == 0)
            return;
        this->elem = new T[s]();
        unsigned int i = 0;
        while (i < this->s){
            this->elem[i] = array[i];
            i++;
        }
    };

    ~Array<T>(){delete [] this->elem;};

    T  &operator[](std::size_t pos) const{
        if (pos >= this->size() || pos < 0)
            throw Array<T>::out_of_range();
        return this->elem[pos];
    };

    Array<T> &operator=(Array<T> const &array){
        delete [] this->elem;
        this->s = array.size();
        if (this->s == 0)
            return(*this);
        this->elem = new T[s]();
        unsigned int i = 0;
        while (i < this->s){
            this->elem[i] = array[i];
            i++;
        }
        return(*this);
    };

    unsigned int size(void) const{return this->s;};

    class out_of_range : public std::exception {
        virtual const char *what() const throw() {return ("Out of range");};
    };

private:
    unsigned int s;
    T *elem;

};


#endif //CPP_MODULES_ARRAY_HPP
