//
// Created by Francois-louis TALLEUX on 7/7/21.
//
#include "string"
#include "iostream"
#include "iter.hpp"


int main(void)
{
    std::cout << "=====TEST FOR INT=====" << std::endl;
    int array_int[5] = {3, 5, 8, 2, 1};
    iter(array_int, 5, write_value);
    std::cout << std::endl;
    std::cout << "====TEST FOR FLOAT====" << std::endl;
    float array_float[5] = {3.14, 5.5, 8.0, 2.2, 1.3};
    iter(array_float, 5, write_value);
    std::cout << std::endl;
    std::cout << "===TEST FOR STRING====" << std::endl;
    std::string array_string[4] = {"FL", "est", "un",  "caillou"};
    iter(array_string, 4, write_value);
    std::cout << std::endl;
}
