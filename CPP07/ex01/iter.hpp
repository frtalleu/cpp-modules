//
// Created by Francois-louis TALLEUX on 7/7/21.
//

#ifndef CPP_MODULES_ITER_HPP
#define CPP_MODULES_ITER_HPP
#include "string"
#include "cctype"
#include "iostream"

template<typename T>
void iter(T *array, size_t size, void (*f)(T const &a))
{
    size_t i = 0;
    while (i < size){
        (*f)(array[i]);
        i++;
    }
}

template<typename T>
void write_value(T const &v)
{
    std::cout << v << " ";
}

#endif //CPP_MODULES_ITER_HPP
