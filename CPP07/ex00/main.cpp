//
// Created by Francois-louis TALLEUX on 7/7/21.
//

#include "whatever.hpp"

int main(){
    {
        int a = 100;
        int b = 50;
        std::cout << "=========FOR INT========" << std::endl;
        std::cout << "At start" << std::endl;
        std::cout << "a = " << a << std::endl;
        std::cout << "b = " << b << std::endl;
        std::cout << "The max is " << ::max(a, b) << std::endl;
        std::cout << "Before swap" << std::endl;
        std::cout << "a = " << a << std::endl;
        std::cout << "b = " << b << std::endl;
        ::swap(a, b);
        std::cout << "After swap" << std::endl;
        std::cout << "a = " << a << std::endl;
        std::cout << "b = " << b << std::endl;
        std::cout << "The min is " << ::min(a, b) << std::endl;
    }
   {
        float a = 10.08;
        float b = 50.09;
        std::cout << "=======FOR FLOAT========" << std::endl;
        std::cout << "At start" << std::endl;
        std::cout << "a = " << a << std::endl;
        std::cout << "b = " << b << std::endl;
        std::cout << "The max is " << max(a, b) << std::endl;
        std::cout << "Before swap" << std::endl;
        std::cout << "a = " << a << std::endl;
        std::cout << "b = " << b << std::endl;
        swap(a, b);
        std::cout << "After swap" << std::endl;
        std::cout << "a = " << a << std::endl;
        std::cout << "b = " << b << std::endl;
        std::cout << "The min is " << min(a, b) << std::endl;
    }
    {
            std::string a = "Bonjour";
            std::string b = "Adieu";
            std::cout << "=======FOR STRING========" << std::endl;
            std::cout << "At start" << std::endl;
            std::cout << "a = " << a << std::endl;
            std::cout << "b = " << b << std::endl;
            std::cout << "The max is " << max(a, b) << std::endl;
            std::cout << "Before swap" << std::endl;
            std::cout << "a = " << a << std::endl;
            std::cout << "b = " << b << std::endl;
            swap(a, b);
            std::cout << "After swap" << std::endl;
            std::cout << "a = " << a << std::endl;
            std::cout << "b = " << b << std::endl;
            std::cout << "The min is " << min(a, b) << std::endl;
    }
}

