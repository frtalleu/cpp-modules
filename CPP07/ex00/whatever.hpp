//
// Created by Francois-louis TALLEUX on 7/7/21.
//

#ifndef CPP_MODULES_WHATEVER_HPP
#define CPP_MODULES_WHATEVER_HPP
#include "iostream"
#include "string"

template< typename T >
T &max(T &a, T &b){
    return ((a>b) ? a : b);
}

template< typename T >
T &min(T &a, T &b){
    return ((a<b) ? a : b);
}

template< typename T >
void swap(T &a, T &b){
    T tmp = a;
    a = b;
    b = tmp;
}

#endif //CPP_MODULES_WHATEVER_HPP
