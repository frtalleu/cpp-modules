//
// Created by Francois-louis TALLEUX on 7/9/21.
//

# include "easyfind.hpp"
# include <vector>
# include <list>


int main() {
    std::vector<int> vec;
    for (int i = 0; i < 10; i++)
        vec.push_back(i * 2);
    std::vector<int>::iterator vec_it = easyfind(vec, 6);
    std::cout << "this is 6 = " << *vec_it << std::endl;
    vec_it++;
    std::cout << "this after 6 = " << *vec_it << std::endl;

    std::list<int> lst;
    for (int i = 0; i < 10; i++)
        lst.push_back(i * 3);
    std::list<int>::iterator lst_it = easyfind(lst, 9);
    std::cout << "this is 9 = " << *lst_it << std::endl;
    lst_it++;
    std::cout << "this after 9 = " << *lst_it << std::endl;
    try {
        vec_it = easyfind(vec, 3);
        (void)vec_it;
    }
    catch (std::exception &e){
        std::cout << e.what() << std::endl;
    }
}