//
// Created by Francois-louis TALLEUX on 7/9/21.
//

#ifndef CPP_MODULES_EASYFIND_HPP
#define CPP_MODULES_EASYFIND_HPP
#include "algorithm"
#include "iostream"
#include "string"

class not_found : public std::exception {
    virtual const char *what() const throw() {return ("Not found :'(");};
};

template<typename T>
typename T::iterator easyfind(T container, int i){
    typename T::iterator tmp = std::find(container.begin(), container.end(), i);
    if(tmp == container.end())
        throw not_found();
    return (tmp);
}

#endif //CPP_MODULES_EASYFIND_HPP
