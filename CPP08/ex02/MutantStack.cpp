//
// Created by frtalleu on 7/10/21.
//

#include "MutantStack.hpp"

template<typename T>
MutantStack<T> &MutantStack<T>::operator=(const MutantStack &src) {
    if (this != src)
        this->c.operator=(src);
    return (*this);
}
