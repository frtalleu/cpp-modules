//
// Created by Francois-louis TALLEUX on 7/10/21.
//

#include "string"
#include "iostream"
#include "span.hpp"

int main()
{
    Span sp = Span(5);
    sp.addNumber(5);
    sp.addNumber(3);
    sp.addNumber(17);
    sp.addNumber(9);
    sp.addNumber(11);
    std::vector<int> ntm;
    for (int i  = 0; i < 30; i++)
        ntm.push_back(i * 3);
    Span sp1(30);
    sp1.addNumber(ntm, 30);
    std::cout << sp.shortestSpan() << std::endl;
    std::cout << sp.longestSpan() << std::endl;
    std::cout << sp1.shortestSpan() << std::endl;
    std::cout << sp1.longestSpan() << std::endl;
    try {
        sp1.addNumber(ntm, 30);
    }
    catch (std::exception &a){
        std::cout << a.what() << std::endl;
    }
}