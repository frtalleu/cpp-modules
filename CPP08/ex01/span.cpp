//
// Created by Francois-louis TALLEUX on 7/9/21.
//

#include "span.hpp"

Span::Span(unsigned int Number) : N(Number){
    this->space_left = Number;
}

void Span::addNumber(int i) {
    if (this->space_left == 0)
        throw no_space();
    this->space_left--;
    this->storage.push_back(i);
    std::sort(this->storage.begin(), this->storage.end());
}

void Span::addNumber(std::vector<int> src, unsigned int size) {
    std::vector<int>::iterator begin = src.begin();
    std::vector<int>::iterator end = src.end();
    unsigned int i = 0;
    while (i < size && begin != end)
    {
        this->addNumber(*begin);
        begin++;
        i++;
    }
}

unsigned int Span::longestSpan() {
    if (this->N - this->space_left <= 1)
        throw no_Span_possible();
    std::vector<int>::iterator iter = this->storage.begin();
    std::vector<int>::iterator iter_max = this->storage.end();
    iter_max--;
    return (static_cast<unsigned int>(*iter_max - *iter));
}

unsigned int Span::shortestSpan() {
    if (this->N - this->space_left <= 1)
        throw no_Span_possible();
    std::vector<int>::iterator iter = this->storage.begin();
    std::vector<int>::iterator iter_next = this->storage.begin();
    iter_next++;
    unsigned int tmp = static_cast<unsigned int>(*iter_next - *iter);
    unsigned int value;
    while (iter_next != this->storage.end()){
        value = static_cast<unsigned int>(*iter - *iter_next);
        if (value < tmp)
            tmp = value;
        iter++;
        iter_next++;
    }
    return (tmp);
}

Span::Span(const Span &src) {
    this->N = src.getN();
    this->space_left = src.space_left;
    this->storage.erase(this->storage.begin(), this->storage.end());
    std::vector<int> tmp = src.get_vector();
    std::vector<int> tmp1 = src.get_vector();
    std::vector<int>::iterator iter = tmp.begin();
    while (iter != tmp1.end()){
        this->storage.push_back(*iter);
        iter++;
    }
}

Span::~Span() {}

Span &Span::operator=(const Span &src) {
    this->N = src.getN();
    this->space_left = src.space_left;
    this->storage.erase(this->storage.begin(), this->storage.end());
    std::vector<int> tmp = src.get_vector();
    std::vector<int> tmp1 = src.get_vector();
    std::vector<int>::iterator iter = tmp.begin();
    while (iter != tmp1.end()){
        this->storage.push_back(*iter);
        iter++;
    }
    return (*this);
}