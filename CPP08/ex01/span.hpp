//
// Created by Francois-louis TALLEUX on 7/9/21.
//

#ifndef CPP_MODULES_SPAN_HPP
#define CPP_MODULES_SPAN_HPP
#include "list"
#include "vector"
#include "string"
#include "iostream"
#include "algorithm"

class Span {
public:
    Span(unsigned int Number);
    Span(const Span &src);
    virtual ~Span();
    void addNumber(int i);
    void addNumber(std::vector<int> src, unsigned int size);
    unsigned int shortestSpan(void);
    unsigned int longestSpan(void);
    Span &operator=(const Span &src);
    class no_space : public std::exception{
        virtual const char *what() const throw() {return ("No more space in storage");};
    };
    class no_Span_possible : public std::exception{
        virtual const char *what() const throw() {return ("No Span possible");};
    };
    unsigned int getN() const {return (this->N);};
    unsigned int get_space_left() const{return (this->space_left);};
    std::vector<int> get_vector() const {return (this->storage);};
private:
    Span(void){};
    unsigned int N;
    unsigned int space_left;
    std::vector<int> storage;
};


#endif //CPP_MODULES_SPAN_HPP