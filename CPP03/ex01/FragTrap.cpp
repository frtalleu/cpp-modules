//
// Created by Francois-louis TALLEUX on 5/12/21.
//

#include "FragTrap.hpp"
#include <string>
#include <iostream>
#include <cstdlib>

FragTrap::FragTrap(std::string names) {
    this->name = names;
    this->max_hit_point = 100;
    this->hit_point = 100;
    this->energy_point = 100;
    this->max_energy_point = 100;
    this->level = 1;
    this->melee_attack_damage = 30;
    this->ranged_attack_damage = 20;
    this->armor_attack_reduction = 5;
    std::cout << "[" << this->name << "] " << "Ha ha ha! I LIVE! Hahaha!" << std::endl;
}

FragTrap::FragTrap(void) {
    this->name = "D3F4UL7";
    this->max_hit_point = 100;
    this->hit_point = 100;
    this->energy_point = 100;
    this->max_energy_point = 100;
    this->level = 1;
    this->melee_attack_damage = 30;
    this->ranged_attack_damage = 20;
    this->armor_attack_reduction = 5;
    std::cout << "[" << this->name << "] " << "Ha ha ha! I LIVE! Hahaha!" << std::endl;
}

FragTrap::FragTrap(const FragTrap &src) {
    this->name = src.get_name();
    this->max_hit_point = src.get_max_hit_point();
    this->hit_point = src.get_hit_point();
    this->energy_point = src.get_energy_point();
    this->max_energy_point = src.get_max_energy_point();
    this->level = this->get_level();
    this->melee_attack_damage = src.get_melee_attack_damage();
    this->ranged_attack_damage = src.get_ranged_attack_damage();
    this->armor_attack_reduction = src.get_armor_attack_reduction();
    std::cout << "[" << this->name << "] " << "Ha ha ha! I LIVE! Hahaha!" << std::endl;
}

FragTrap &FragTrap::operator=(const FragTrap &src) {
    this->name = src.get_name();
    this->max_hit_point = src.get_max_hit_point();
    this->hit_point = src.get_hit_point();
    this->energy_point = src.get_energy_point();
    this->max_energy_point = src.get_max_energy_point();
    this->level = this->get_level();
    this->melee_attack_damage = src.get_melee_attack_damage();
    this->ranged_attack_damage = src.get_ranged_attack_damage();
    this->armor_attack_reduction = src.get_armor_attack_reduction();
    return (*this);
}

std::string FragTrap::get_name() const {
    return (this->name);
}

unsigned int FragTrap::get_max_hit_point() const {
    return (this->max_hit_point);
}

unsigned int FragTrap::get_hit_point() const {
    return (this->hit_point);
}

unsigned int FragTrap::get_energy_point() const {
    return (this->energy_point);
}

unsigned int FragTrap::get_max_energy_point() const {
    return (this->max_energy_point);
}

unsigned int FragTrap::get_level() const {
    return (this->level);
}

unsigned int FragTrap::get_melee_attack_damage() const {
    return (this->melee_attack_damage);
}

unsigned int FragTrap::get_ranged_attack_damage() const {
    return (this->ranged_attack_damage);
}

unsigned int FragTrap::get_armor_attack_reduction() const {
    return (this->armor_attack_reduction);
}

FragTrap::~FragTrap(void) {
    std::cout << "[" << this->name << "] " << "I'll die the way I lived: annoying!" << std::endl;
}

unsigned  int FragTrap::rangedAttack(std::string const &target) {
    std::cout << "FR4G-TR4P " << this->name << " attaque " << target << " à distance, causant " << this->ranged_attack_damage <<  " points de dégâts !" << std::endl;
    return (this->ranged_attack_damage);
}

unsigned  int FragTrap::meleeAttack(std::string const &target) {
    std::cout << "FR4G-TR4P " << this->name << " attaque " << target << " au corps à corps, causant " << this->ranged_attack_damage <<  " points de dégâts !" << std::endl;
    return (this->ranged_attack_damage);
}

void FragTrap::takeDamage(unsigned int amount) {
    std::cout << "[" << this->name << "] \"No, nononono NO!\" ";
    unsigned int j = 0;
    if (amount < this->armor_attack_reduction)
        j = 0;
    else if (amount - this->armor_attack_reduction > this->hit_point) {
        j = amount - this->armor_attack_reduction;
        hit_point = 0;
    } else {
        j = amount - this->armor_attack_reduction;
        this->hit_point = this->hit_point - (amount - this->armor_attack_reduction);
    }
    std::cout << j << " damages taken after armor reduction" << std::endl;
    if (this->hit_point == 0)
        std::cout << "[" << this->name << "] IAM DEAD" << std::endl;
}

void FragTrap::beRepaired(unsigned int amount) {
    this->hit_point += amount;
    if (this->hit_point > this->max_hit_point)
        this->hit_point =  this->max_hit_point;
    std::cout << "[" << this->name << "] Health over here!" << std::endl;
}

unsigned int FragTrap::vaulthunter_dot_exe(const std::string &target) {
    srand(time(0));
    int attack = std::rand() % 5;
    unsigned int amount = 0;
    if (this->energy_point < 25)
    {
        std::cout << "[" << this->name << "] \"Where'd all my bullets go?\"" << std::endl;
        return (0);
    }
    else if (attack == 0) {
        amount = 10;
        std::cout << "[" << this->name << "] \"I will prove to you my robotic superiority!\"" << std::endl;
    }
    else if (attack == 1){
        amount = 5;
        std::cout << "[" << this->name << "] \"Woohoo! In your face!\"" << std::endl;
    }
    else if (attack == 2){
        amount = 15;
        std::cout << "[" << this->name << "] \"I am so impressed with myself!\"" << std::endl;
    }
    else if (attack == 3){
        amount = 35;
        std::cout << "[" << this->name << "] \"Aw yeah!\"" << std::endl;
    }
    else if (attack == 4){
        amount = 105;
        std::cout << "[" << this->name << "] \"Who's a badass robot? This guy!\"" << std::endl;
    }
    std::cout << this->name << " gives "<< amount << " damages to " << target << std::endl;
    this->energy_point -= 25;
    return (amount);
}