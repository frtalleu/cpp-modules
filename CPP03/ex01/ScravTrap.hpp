//
// Created by Francois-louis TALLEUX on 5/12/21.
//

#ifndef CPP_ScravTrap_HPP
#define CPP_ScravTrap_HPP
#include <iostream>
#include <string>

class ScravTrap {
    private:
        unsigned int hit_point;
        unsigned int max_hit_point;
        unsigned int energy_point;
        unsigned int max_energy_point;
        unsigned int level;
        std::string name;
        unsigned int melee_attack_damage;
        unsigned int ranged_attack_damage;
        unsigned int armor_attack_reduction;
    public:
        ScravTrap(void);
        ScravTrap(std::string names);
        ScravTrap(const ScravTrap &src);
        ~ScravTrap(void);
        unsigned int rangedAttack(std::string const& target);
        unsigned int meleeAttack(std::string const& target);
        void challengeNewComer(std::string const& target);
        void takeDamage(unsigned int amount);
        void beRepaired (unsigned int amount);
        ScravTrap &operator=(ScravTrap const &rhs);
        unsigned int get_hit_point(void) const;
        unsigned int get_max_hit_point(void) const;
        unsigned int get_energy_point(void) const;
        unsigned int get_max_energy_point(void) const;
        unsigned int get_level(void) const;
        std::string get_name(void) const;
        unsigned int get_melee_attack_damage(void) const;
        unsigned int get_ranged_attack_damage(void) const;
        unsigned int get_armor_attack_reduction(void) const;
};


#endif //CPP_ScravTrap_HPP