//
// Created by Francois-louis TALLEUX on 5/19/21.
//

#ifndef CPP_MODULES_NINJATRAP_HPP
#define CPP_MODULES_NINJATRAP_HPP
#include "ClapTrap.hpp"
#include "ScravTrap.hpp"
#include "FragTrap.hpp"
#include "string"
#include "iostream"

class NinjaTrap : public ClapTrap{
    public:
        NinjaTrap(void);
        NinjaTrap(std::string names);
        NinjaTrap(const NinjaTrap &src);
        ~NinjaTrap();
        void ninjaShoebox(const FragTrap &src);
        void ninjaShoebox(const ScravTrap &src);
        void ninjaShoebox(const ClapTrap &src);
        void ninjaShoebox(const NinjaTrap &src);
        NinjaTrap &operator=(const NinjaTrap &src);
};


#endif //CPP_MODULES_NINJATRAP_HPP
