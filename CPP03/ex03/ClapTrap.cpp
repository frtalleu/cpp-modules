//
// Created by Francois-louis TALLEUX on 5/18/21.
//

#include "ClapTrap.hpp"
#include <iostream>
#include <string>

ClapTrap::ClapTrap() {
    this->name = "D3F4UL7";
    this->max_hit_point = 100;
    this->hit_point = 100;
    this->energy_point = 100;
    this->max_energy_point = 100;
    this->level = 1;
    this->melee_attack_damage = 50;
    this->ranged_attack_damage = 50;
    this->armor_attack_reduction =  10;
}

ClapTrap::ClapTrap(std::string names) {
    this->name = names;
    this->max_hit_point = 100;
    this->hit_point = 100;
    this->energy_point = 100;
    this->max_energy_point = 100;
    this->level = 1;
    this->melee_attack_damage = 30;
    this->ranged_attack_damage = 20;
    this->armor_attack_reduction = 5;
}

ClapTrap::ClapTrap(const ClapTrap &src) {
    this->name = src.get_name();
    this->max_hit_point = src.get_max_hit_point();
    this->hit_point = src.get_hit_point();
    this->energy_point = src.get_energy_point();
    this->max_energy_point = src.get_max_energy_point();
    this->level = this->get_level();
    this->melee_attack_damage = src.get_melee_attack_damage();
    this->ranged_attack_damage = src.get_ranged_attack_damage();
    this->armor_attack_reduction = src.get_armor_attack_reduction();
}

ClapTrap::~ClapTrap() {
    std::cout << "DEAD" << std::endl;
}

std::string ClapTrap::get_name() const {
    return (this->name);
}

unsigned int ClapTrap::get_max_hit_point() const {
    return (this->max_hit_point);
}

unsigned int ClapTrap::get_hit_point() const {
    return (this->hit_point);
}

unsigned int ClapTrap::get_energy_point() const {
    return (this->energy_point);
}

unsigned int ClapTrap::get_max_energy_point() const {
    return (this->max_energy_point);
}

unsigned int ClapTrap::get_level() const {
    return (this->level);
}

unsigned int ClapTrap::get_melee_attack_damage() const {
    return (this->melee_attack_damage);
}

unsigned int ClapTrap::get_ranged_attack_damage() const {
    return (this->ranged_attack_damage);
}

unsigned int ClapTrap::get_armor_attack_reduction() const {
    return (this->armor_attack_reduction);
}

ClapTrap &ClapTrap::operator=(const ClapTrap &src) {
    this->name = src.get_name();
    this->max_hit_point = src.get_max_hit_point();
    this->hit_point = src.get_hit_point();
    this->energy_point = src.get_energy_point();
    this->max_energy_point = src.get_max_energy_point();
    this->level = this->get_level();
    this->melee_attack_damage = src.get_melee_attack_damage();
    this->ranged_attack_damage = src.get_ranged_attack_damage();
    this->armor_attack_reduction = src.get_armor_attack_reduction();
    return (*this);
}

void ClapTrap::beRepaired(unsigned int amount) {
    this->hit_point += amount;
    if (this->hit_point > this->max_hit_point)
        this->hit_point =  this->max_hit_point;
    std::cout << "[" << this->name << "] Sweet life juice!" << std::endl;
}

void ClapTrap::takeDamage(unsigned int amount) {
    std::cout << "[" << this->name << "] \"Oh no, Badass!\" ";
    unsigned int j;
    if (amount < this->armor_attack_reduction)
        j = 0;
    else if (amount - this->armor_attack_reduction > this->hit_point) {
        j = amount - this->armor_attack_reduction;
        hit_point = 0;
    } else {
        j = amount - this->armor_attack_reduction;
        this->hit_point = this->hit_point - (amount - this->armor_attack_reduction);
    }
    std::cout << j << " damages taken after armor reduction" << std::endl;
    if (this->hit_point == 0)
        std::cout << "[" << this->name << "] I AM DEAD, because of you!" << std::endl;
}

unsigned int ClapTrap::rangedAttack(const std::string &target) {
    std::cout << "CL4P-TR4P " << this->name << " attaque " << target << " au lance pierre, causant " << this->ranged_attack_damage <<  " points de dégâts !" << std::endl;
    return (this->ranged_attack_damage);
}

unsigned int ClapTrap::meleeAttack(const std::string &target) {
    std::cout << "FR4G-TP " << this->name << " attaque " << target << " au poing americain, causant " << this->ranged_attack_damage <<  " points de dégâts !" << std::endl;
    return (this->ranged_attack_damage);
}