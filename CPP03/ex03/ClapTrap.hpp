//
// Created by Francois-louis TALLEUX on 5/18/21.
//

#ifndef CPP_MODULES_CLAPTRAP_HPP
#define CPP_MODULES_CLAPTRAP_HPP
#include <iostream>
#include <string>

class ClapTrap {
    protected:
        unsigned int hit_point;
        unsigned int max_hit_point;
        unsigned int energy_point;
        unsigned int max_energy_point;
        unsigned int level;
        std::string name;
        unsigned int melee_attack_damage;
        unsigned int ranged_attack_damage;
        unsigned int armor_attack_reduction;
    public:
        ClapTrap(void);
        ClapTrap(std::string names);
        ClapTrap(const ClapTrap &src);
        ~ClapTrap(void);
        ClapTrap &operator=(const ClapTrap &src);
        unsigned int get_hit_point(void) const;
        unsigned int get_max_hit_point(void) const;
        unsigned int get_energy_point(void) const;
        unsigned int get_max_energy_point(void) const;
        unsigned int get_level(void) const;
        std::string get_name(void) const;
        unsigned int get_melee_attack_damage(void) const;
        unsigned int get_ranged_attack_damage(void) const;
        unsigned int get_armor_attack_reduction(void) const;
        void takeDamage(unsigned int amount);
        void beRepaired (unsigned int amount);
        unsigned int rangedAttack(std::string const& target);
        unsigned int meleeAttack(std::string const& target);
};


#endif //CPP_MODULES_CLAPTRAP_HPP