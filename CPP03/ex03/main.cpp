//
// Created by Francois-louis TALLEUX on 5/13/21.
//

#include <iostream>
#include <string>
#include "FragTrap.hpp"
#include "ClapTrap.hpp"
#include "ScravTrap.hpp"
#include "NinjaTrap.hpp"

int main(){
    srand(time(0));
    ScravTrap bot_twin1("TW1N1-TR4P");
    ScravTrap bot_twin2("TW1N2-TR4P");
    NinjaTrap bot1("N1Nj4-7R4P")
    int i;
    int j;
    int k;
    int l;
    int target;

    while (bot1.get_hit_point() != 0 && bot_twin1.get_hit_point() != 0){
        i = std::rand() % 3;
        j = std::rand() % 3;
        k = std::rand() % 2;
        l = std::rand() % 2;
        target = std::rand() % 2;
        if (k == 0)
            bot1.beRepaired(std::rand() % 30);
        if (l == 0) {
            bot_twin1.beRepaired(std::rand() % 30);
            bot_twin2.beRepaired(std::rand() % 30);
        }
        if (i == 0 && bot1.get_hit_point() != 0 && bot_twin1.get_hit_point() != 0 && bot_twin1.get_hit_point() != 0) {
            bot1.takeDamage(bot_twin1.rangedAttack(bot1.get_name()));
            bot1.takeDamage(bot_twin2.rangedAttack(bot1.get_name()));
        }
        else if (i == 1 && bot1.get_hit_point() != 0 && bot_twin1.get_hit_point() != 0 && bot_twin1.get_hit_point() != 0) {
            bot1.takeDamage(bot_twin1.meleeAttack(bot1.get_name()));
            bot1.takeDamage(bot_twin2.meleeAttack(bot1.get_name()));
        }
        else if (i == 2 && bot1.get_hit_point() != 0 && bot_twin1.get_hit_point() != 0 && bot_twin1.get_hit_point() != 0) {
            bot_twin1.challengeNewComer(bot1.get_name());
            bot_twin2.challengeNewComer(bot1.get_name());
        }

        if (j == 0 && bot1.get_hit_point() != 0 ) {
            if (target == 1 && bot_twin1.get_hit_point() != 0)
                bot_twin1.takeDamage(bot1.rangedAttack(bot_twin1.get_name()));
            else if (bot_twin2.get_hit_point() != 0)
                bot_twin2.takeDamage(bot1.rangedAttack(bot_twin2.get_name()));
        }
        else if (j == 1 && bot1.get_hit_point() != 0) {
            if (target == 1 && bot_twin1.get_hit_point() != 0)
                bot_twin1.takeDamage(bot1.meleeAttack(bot_twin1.get_name()));
            else if (bot_twin2.get_hit_point() != 0)
                bot_twin2.takeDamage(bot1.meleeAttack(bot_twin2.get_name()));
        }
        else if (j == 2 && bot_twin1.get_hit_point()) {
            if (target == 1 && bot_twin1.get_hit_point() != 0)
                bot1.ninjaShoebox(bot_twin2);
            else if (bot_twin2.get_hit_point() != 0)
                bot1.ninjaShoebox(bot_twin1);
        }
    }
}