//
// Created by Francois-louis TALLEUX on 5/13/21.
//

#include <iostream>
#include <string>
#include "FragTrap.hpp"

int main(){
    srand(time(0));
    FragTrap bot1("FR4G-7R4P");
    FragTrap bot2("FR4G-7R4P-815");
    int i;
    int j;
    int k;
    int l;

    while (bot1.get_hit_point() != 0 && bot2.get_hit_point() != 0){
        i = std::rand() % 3;
        j = std::rand() % 3;
        k = std::rand() % 2;
        l = std::rand() % 2;

        if (k == 0)
            bot1.beRepaired(std::rand() % 30);
        if (l == 0)
            bot2.beRepaired(std::rand() % 30);
        if (i == 0 && bot1.get_hit_point() != 0 && bot2.get_hit_point() != 0)
            bot1.takeDamage(bot2.rangedAttack(bot1.get_name()));
        else if (i == 1 && bot1.get_hit_point() != 0 && bot2.get_hit_point() != 0)
            bot1.takeDamage(bot2.meleeAttack(bot1.get_name()));
        else if (i == 2 && bot1.get_hit_point() != 0 && bot2.get_hit_point() != 0)
            bot1.takeDamage(bot2.vaulthunter_dot_exe(bot1.get_name()));
        if (j == 0 && bot2.get_hit_point() != 0 && bot1.get_hit_point() != 0)
            bot2.takeDamage(bot1.rangedAttack(bot2.get_name()));
        else if (j == 1 && bot2.get_hit_point() != 0 && bot1.get_hit_point() != 0)
            bot2.takeDamage(bot1.meleeAttack(bot2.get_name()));
        else if (j == 2 && bot2.get_hit_point() != 0 && bot2.get_hit_point())
            bot2.takeDamage(bot1.vaulthunter_dot_exe(bot2.get_name()));
    }
}