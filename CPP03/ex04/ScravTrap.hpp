//
// Created by Francois-louis TALLEUX on 5/12/21.
//

#ifndef CPP_ScravTrap_HPP
#define CPP_ScravTrap_HPP
#include <iostream>
#include <string>
#include "ClapTrap.hpp"

class ScravTrap : public ClapTrap{
    public:
        ScravTrap(void);
        ScravTrap(std::string names);
        ScravTrap(const ScravTrap &src);
        ~ScravTrap(void);
        unsigned int rangedAttack(std::string const& target);
        unsigned int meleeAttack(std::string const& target);
        void challengeNewComer(std::string const& target);
        void takeDamage(unsigned int amount);
        void beRepaired (unsigned int amount);
        ScravTrap &operator=(ScravTrap const &rhs);
};


#endif //CPP_ScravTrap_HPP