//
// Created by Francois-louis TALLEUX on 5/20/21.
//

#include "SuperTrap.hpp"
#include "ScravTrap.hpp"
#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "NinjaTrap.hpp"


SuperTrap::SuperTrap(std::string names) : ClapTrap(names), FragTrap(names), NinjaTrap(names){
    this->max_hit_point = this->FragTrap::max_hit_point;
    this->hit_point = FragTrap::hit_point;
    this->energy_point = NinjaTrap::energy_point;
    this->max_energy_point = NinjaTrap::max_hit_point;
    this->level = 1;
    this->melee_attack_damage = NinjaTrap::melee_attack_damage;
    this->ranged_attack_damage = FragTrap::ranged_attack_damage;
    this->armor_attack_reduction = FragTrap::armor_attack_reduction;
    std::cout << "[" << this->name << "] " << "Ha ha ha! I LIVE! Hohoho!" << std::endl;
    std::cout << "=>>>>>>>>>>>>>>>>> " << max_hit_point << std::endl;
}

SuperTrap::SuperTrap() : ClapTrap(), FragTrap(), NinjaTrap(){
    this->name = "D3F4UL7";
    this->max_hit_point = FragTrap::max_hit_point;
    this->hit_point = FragTrap::hit_point;
    this->energy_point = NinjaTrap::energy_point;
    this->max_energy_point = NinjaTrap::max_hit_point;
    this->level = 1;
    this->melee_attack_damage = NinjaTrap::melee_attack_damage;
    this->ranged_attack_damage = FragTrap::ranged_attack_damage;
    this->armor_attack_reduction = FragTrap::armor_attack_reduction;
    std::cout << "[" << this->name << "] " << "Ha ha ha! I LIVE! Hahaha!" << std::endl;
}

SuperTrap::SuperTrap(const SuperTrap &src) {
    this->name = src.get_name();
    this->max_hit_point = src.get_max_hit_point();
    this->hit_point = src.get_hit_point();
    this->energy_point = src.get_energy_point();
    this->max_energy_point = src.get_max_energy_point();
    this->level = this->get_level();
    this->melee_attack_damage = src.get_melee_attack_damage();
    this->ranged_attack_damage = src.get_ranged_attack_damage();
    this->armor_attack_reduction = src.get_armor_attack_reduction();
    std::cout << "[" << this->name << "] " << "Ha ha ha! I LIVE! Hahaha!" << std::endl;
}

SuperTrap::~SuperTrap() {
    std::cout << "[" << this->name << " ] I SUPER DIE" << std::endl;
}