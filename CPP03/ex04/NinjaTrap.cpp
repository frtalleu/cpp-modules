//
// Created by Francois-louis TALLEUX on 5/19/21.
//

#include "NinjaTrap.hpp"
#include "iostream"
#include "string"

NinjaTrap::NinjaTrap(void) {
    this->name = "D3F4UL7";
    this->max_hit_point = 1;
    this->hit_point = 100;
    this->energy_point = 100;
    this->max_energy_point = 100;
    this->level = 1;
    this->melee_attack_damage = 30;
    this->ranged_attack_damage = 20;
    this->armor_attack_reduction = 5;
    std::cout << "[" << this->name << "] " << "Ha ha ha! I LIVE, FOR THE FIRST TIME!" << std::endl;
}

NinjaTrap::NinjaTrap(std::string names) {
    this->name = names;
    this->max_hit_point = 1;
    this->hit_point = 100;
    this->energy_point = 100;
    this->max_energy_point = 100;
    this->level = 1;
    this->melee_attack_damage = 30;
    this->ranged_attack_damage = 20;
    this->armor_attack_reduction = 5;
    std::cout << "[" << this->name << "] " << "Ha ha ha! I LIVE, FOR THE FIRST TIME!" << std::endl;
}

NinjaTrap::NinjaTrap(const NinjaTrap &src) {
    this->name = src.get_name();
    this->max_hit_point = src.get_max_hit_point();
    this->hit_point = src.get_hit_point();
    this->energy_point = src.get_energy_point();
    this->max_energy_point = src.get_max_energy_point();
    this->level = this->get_level();
    this->melee_attack_damage = src.get_melee_attack_damage();
    this->ranged_attack_damage = src.get_ranged_attack_damage();
    this->armor_attack_reduction = src.get_armor_attack_reduction();
    std::cout << "[" << this->name << "] " << "Ha ha ha! I LIVE, FOR THE FIRST TIME!" << std::endl;
}

NinjaTrap::~NinjaTrap() {
    std::cout << "mdr chui mort" << std::endl;
}

void NinjaTrap::ninjaShoebox(const ClapTrap &src) {
    std::cout << "Oh this is a ClapTrap his name is " << src.get_name() <<  std::endl;
}

void NinjaTrap::ninjaShoebox(const FragTrap &src) {
    std::cout << "Oh this is a FragTrap his name is " << src.get_name() << std::endl;
}

void NinjaTrap::ninjaShoebox(const ScravTrap &src) {
    std::cout << "Oh this is a SravTrap his name is " << src.get_name() << std::endl;
}

void NinjaTrap::ninjaShoebox(const NinjaTrap &src) {
    std::cout << "Oh this is a NinjaTrap his name is " << src.get_name() << std::endl;
}

NinjaTrap &NinjaTrap::operator=(const NinjaTrap &src) {
    this->name = src.get_name();
    this->max_hit_point = src.get_max_hit_point();
    this->hit_point = src.get_hit_point();
    this->energy_point = src.get_energy_point();
    this->max_energy_point = src.get_max_energy_point();
    this->level = this->get_level();
    this->melee_attack_damage = src.get_melee_attack_damage();
    this->ranged_attack_damage = src.get_ranged_attack_damage();
    this->armor_attack_reduction = src.get_armor_attack_reduction();
    return (*this);
}