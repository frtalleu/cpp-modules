//
// Created by Francois-louis TALLEUX on 5/12/21.
//

#ifndef CPP_FRAGTRAP_HPP
#define CPP_FRAGTRAP_HPP
#include <iostream>
#include <string>
#include "ClapTrap.hpp"

class FragTrap : virtual public ClapTrap {

    public:
        FragTrap(void);
        FragTrap(std::string names);
        FragTrap(const FragTrap &src);
        ~FragTrap(void);
        unsigned int rangedAttack(std::string const& target);
        unsigned int meleeAttack(std::string const& target);
        unsigned int vaulthunter_dot_exe(std::string const& target);
        void takeDamage(unsigned int amount);
        void beRepaired (unsigned int amount);
        FragTrap &operator=(FragTrap const &rhs);
};


#endif //CPP_FRAGTRAP_HPP
