//
// Created by Francois-louis TALLEUX on 5/12/21.
//

#include "ScravTrap.hpp"
#include <string>
#include <iostream>
#include <cstdlib>
#include "ClapTrap.hpp"

ScravTrap::ScravTrap(std::string names) {
    this->name = names;
    this->max_hit_point = 100;
    this->hit_point = 100;
    this->energy_point = 50;
    this->max_energy_point = 50;
    this->level = 1;
    this->melee_attack_damage = 20;
    this->ranged_attack_damage = 15;
    this->armor_attack_reduction = 3;
    std::cout << "[" << this->name << "] " << "Hahahahaha! I'm alive!" << std::endl;
}

ScravTrap::ScravTrap(void) {
    this->name = "D3F4UL7";
    this->max_hit_point = 100;
    this->hit_point = 100;
    this->energy_point = 50;
    this->max_energy_point = 50;
    this->level = 1;
    this->melee_attack_damage = 20;
    this->ranged_attack_damage = 15;
    this->armor_attack_reduction = 3;
    std::cout << "[" << this->name << "] " << "Hahahahaha! I'm alive!" << std::endl;
}

ScravTrap::ScravTrap(const ScravTrap &src) {
    this->name = src.get_name();
    this->max_hit_point = src.get_max_hit_point();
    this->hit_point = src.get_hit_point();
    this->energy_point = src.get_energy_point();
    this->max_energy_point = src.get_max_energy_point();
    this->level = this->get_level();
    this->melee_attack_damage = src.get_melee_attack_damage();
    this->ranged_attack_damage = src.get_ranged_attack_damage();
    this->armor_attack_reduction = src.get_armor_attack_reduction();
    std::cout << "[" << this->name << "] " << "Hahahahaha! I'm alive!" << std::endl;
}

ScravTrap &ScravTrap::operator=(const ScravTrap &src) {
    this->name = src.get_name();
    this->max_hit_point = src.get_max_hit_point();
    this->hit_point = src.get_hit_point();
    this->energy_point = src.get_energy_point();
    this->max_energy_point = src.get_max_energy_point();
    this->level = this->get_level();
    this->melee_attack_damage = src.get_melee_attack_damage();
    this->ranged_attack_damage = src.get_ranged_attack_damage();
    this->armor_attack_reduction = src.get_armor_attack_reduction();
    return (*this);
}

ScravTrap::~ScravTrap(void) {
    std::cout << "[" << this->name << "] " << "I'M DEAD I'M DEAD OHMYGOD I'M DEAD!" << std::endl;
}

unsigned  int ScravTrap::rangedAttack(std::string const &target) {
    std::cout << "SCR4V-TR4P " << this->name << " tire avec son arc en plastiqeu sur " << target << ", lui infligeant " << this->ranged_attack_damage <<  " points de degats !" << std::endl;
    return (this->ranged_attack_damage);
}

unsigned  int ScravTrap::meleeAttack(std::string const &target) {
    std::cout << "SCR4V-TR4P " << this->name << " gifle " << target << ", lui infligeant " << this->ranged_attack_damage <<  " points de degats !" << std::endl;
    return (this->ranged_attack_damage);
}

void ScravTrap::takeDamage(unsigned int amount) {
    std::cout << "[" << this->name << "] \"Oh no, Badass!\" ";
    unsigned int j;
    if (amount < this->armor_attack_reduction)
        j = 0;
    else if (amount - this->armor_attack_reduction > this->hit_point) {
        j = amount - this->armor_attack_reduction;
        hit_point = 0;
    } else {
        j = amount - this->armor_attack_reduction;
        this->hit_point = this->hit_point - (amount - this->armor_attack_reduction);
    }
    std::cout << j << " damages taken after armor reduction" << std::endl;
    if (this->hit_point == 0)
        std::cout << "[" << this->name << "] The robot is dead" << std::endl;
}

void ScravTrap::beRepaired(unsigned int amount) {
    this->hit_point += amount;
    if (this->hit_point > this->max_hit_point)
        this->hit_point =  this->max_hit_point;
    std::cout << "[" << this->name << "] I found health!" << std::endl;
}

void ScravTrap::challengeNewComer(const std::string &target) {
    srand(time(0));
    int attack = std::rand() % 5;
    std::cout << this->name << " engage un defi avec " << target << std::endl;
    if (attack == 0)
        std::cout << "[" << this->name << "] \" On va faire un pierre feuille ciseaux\" to " << target << std::endl;
    else if (attack == 1)
        std::cout << "[" << this->name << "] \"On va faire une course sur 999999km\"" << std::endl;
    else if (attack == 2)
        std::cout << "[" << this->name << "] \"Petit bras de fer\"" << std::endl;
    else if (attack == 3)
        std::cout << "[" << this->name << "] \"Petite roulette russe avec un chargeur plein\"" << std::endl;
    else if (attack == 4)
        std::cout << "[" << this->name << "] \"DUEEEEEEEEEEEEEEEL\"" << std::endl;
}