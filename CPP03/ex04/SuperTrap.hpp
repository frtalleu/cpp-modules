//
// Created by Francois-louis TALLEUX on 5/20/21.
//

#ifndef CPP_MODULES_SUPERTRAP_HPP
#define CPP_MODULES_SUPERTRAP_HPP
#include "iostream"
#include "string"
#include "FragTrap.hpp"
#include "ScravTrap.hpp"
#include "NinjaTrap.hpp"
#include "ClapTrap.hpp"


class SuperTrap : virtual public FragTrap, virtual public NinjaTrap {
    public:
        SuperTrap(std::string names);
        SuperTrap();
        SuperTrap(const SuperTrap &src);
        ~SuperTrap();
        using ClapTrap::operator=;
        using FragTrap::rangedAttack;
        using NinjaTrap::meleeAttack;
};


#endif //CPP_MODULES_SUPERTRAP_HPP
