//
// Created by Francois-louis TALLEUX on 5/13/21.
//

#include <iostream>
#include <string>
#include "FragTrap.hpp"
#include "ClapTrap.hpp"
#include "ScravTrap.hpp"
#include "SuperTrap.hpp"

int main(){
    srand(time(0));
    SuperTrap super1("SUPER1");
    SuperTrap super2("SUPER2");
    std::cout << super1.get_max_hit_point() << " OOOOOOOOOOOOOOOOOOOOOOOOO  "<< std::endl;
    ClapTrap useless_bot("USELESS_BOT");
    int attack1;
    int attack2;

    while (super1.get_hit_point() != 0 && super2.get_hit_point() != 0){

        if (super1.get_energy_point() >=25)
            attack1 = std::rand() % 5;
        else
            attack1 = std::rand() % 4;
        if (super2.get_energy_point() >= 25)
            attack2 = std::rand() % 5;
        else
            attack2 = std::rand() % 4;
        if (attack1 == 0)
            super1.beRepaired(std::rand() % 30);
        if (attack1 == 1)
            super2.takeDamage(super1.meleeAttack(super2.get_name()));
        if (attack1 == 2)
            super2.takeDamage(super1.rangedAttack(super2.get_name()));
        if (attack1 == 4)
            super1.vaulthunter_dot_exe(super2.get_name());
        if (attack1 == 3)
            super1.ninjaShoebox(useless_bot);

        if (attack2 == 0)
            super2.beRepaired(std::rand() % 30);
        if (attack2 == 1)
            super1.takeDamage(super2.meleeAttack(super1.get_name()));
        if (attack2 == 2)
            super1.takeDamage(super2.rangedAttack(super1.get_name()));
        if (attack2 == 4)
            super2.vaulthunter_dot_exe(super1.get_name());
        if (attack2 == 3)
            super2.ninjaShoebox(useless_bot);

    }
}